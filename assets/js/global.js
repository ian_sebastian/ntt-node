//NOTE:This file uses webpack so that we can use these libraries in the client side

window.CompareString = require('string-similarity')

window._ = require('lodash')

window.originalImage = undefined;
window.originalImageFlip = false;


window.browserInfo = require('detect-browser').detect();
console.log(browserInfo.name)


window.IOSversion = () => {
    if (/iP(hone|od|ad)/.test(navigator.platform)) {
        // supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
        var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
        // return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
        return v[1];
    }
}

window.shouldUserAlternative = () => {
    return (IOSversion() === '10') || browserInfo.name === 'crios'
}
