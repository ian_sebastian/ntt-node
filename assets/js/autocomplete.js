
window.awesomplete = {
    workLocation: undefined,
    companyList: undefined,
    workLocationSave: undefined,
    companyListSave: undefined,
}

// window.initializeAwesomplete;


$(function () {
    window.initializeAwesomplete = function (input, list, ac) {
        if (awesomplete[ac] !== undefined) {
            awesomplete[ac].destroy();
        }


        var input = $(input);
        var datalist = $(list);
        input.off('click');
        input.click(function () {
            if (awesomplete[ac] !== undefined) {
                if (!awesomplete[ac].isOpened) {
                    awesomplete[ac].open();
                } else {
                    awesomplete[ac].close();
                }
            }
        });

        awesomplete[ac] = new Awesomplete(input[0], {
            list: datalist[0],
            minChars: 1,
            maxItems: 300,
            sort: function (a, b) {
                if (a == LANGUAGES[currentLocale].COMPANIES.company_1) return -1;
                if (b == LANGUAGES[currentLocale].COMPANIES.company_1) return 1;
                if (a.toLowerCase() < b.toLowerCase()) return -1;
                if (a.toLowerCase() > b.toLowerCase()) return 1;
                return 0;
            },
            // filter: Awesomplete.FILTER_STARTSWITH
        });
        input.off('awesomplete-open');
        input.on('awesomplete-open', function (e) {

            if (_.isEmpty($(this).val())) {
                awesomplete[ac].suggestions = [];
                var list = getDataList(datalist.children());
                var UL = $(awesomplete[ac].ul);
                var li;

                UL.empty();
                for (var i = 0; i < list.length; i++) {
                    li = document.createElement('li');
                    li.setAttribute('aria-selected', false);
                    li.setAttribute('data-thing', list[i]);
                    li.textContent = list[i];
                    UL.append(li);
                    awesomplete[ac].suggestions.push({
                        label: list[i],
                        value: list[i]
                    });
                }
            }

        });


        if (browserInfo.name === 'ie') {
            $(document).click(function (e) {
                var mX = e.pageX;
                var mY = e.pageY;
                var distance = calculateDistance(input, mX, mY);
                console.log(distance);
                if (distance > 180) {
                    awesomplete[ac].close({
                        reason: 'force'
                    });
                }

            });
        }


    }


    initializeAwesomplete('#workingLocationSearchList', '#workingLocationList', 'workLocation');
    initializeAwesomplete('#companyArchiveSearchList', '#companyDatalist', 'companyList');


    if ($('.home').length === 0) {
        initializeAwesomplete('#workingLocationSaveList', '#workingLocationListSave', 'workLocationSave');
        initializeAwesomplete('#companySearchList', '#companyDatalist', 'companyListSave');
    }



    function calculateDistance(elem, mouseX, mouseY) {
        return Math.floor(Math.sqrt(Math.pow(mouseX - (elem.offset().left + (elem.width() / 2)), 2) + Math.pow(mouseY - (elem.offset().top + (elem.height() / 2)), 2)));
    }

    function getDataList(list) {
        return list.map(function () {
            return $(this).attr('name');
        }).get();
    }

});