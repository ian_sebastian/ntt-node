


$(function () {
    var width = 1000;
    var height = 1000;

    var $uploadBtn = $('#btn-upload');
    var $imgSource;
    var originalImageData;
    var promiseChecker;

    $binCanvas = $('#div-canvas_filter canvas')[0];
    $binCanvas.width = width;
    $binCanvas.height = height;
    binCanvasCtx = $binCanvas.getContext('2d');



    $uploadBtn.click(function () {

        toBluePromise($binCanvas.toDataURL()).then(function (imageSource) {
            if (promiseChecker !== undefined) {
                if (!promiseChecker.isPending()) {
                    promiseChecker = uploadImagePromise(imageSource);
                }
            } else {
                promiseChecker = uploadImagePromise(imageSource);
            }

            $('.loader__page').show();
            $('.filter__page').hide();
            $('html,body').animate({scrollTop:0},0);
        }).catch(function (err) {
            console.log(err);
        });

    });




    function copyImageData(ctx, src) {
        var dst = ctx.createImageData(src.width, src.height);
        dst.data.set(src.data);
        return dst;
    }


    function toBluePromise(source) {

        return new Promise(function (resolve, reject) {
            var image = new Image();
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');

            image.onload = function (e) {
                var targetImage = e.target;


                canvas.width = width;
                canvas.height = height;

                ctx.drawImage(targetImage, 0, 0, width, height);


                var imageData = ctx.getImageData(0, 0, width, height);

                for (var i = imageData.data.length; i >= 0; i -= 4) {
                    var r = imageData.data[i];
                    var g = imageData.data[i + 1];
                    var b = imageData.data[i + 2];
                    var a = imageData.data[i + 3];

                    if (r === 0 && g === 0 && b === 0) {
                        imageData.data[i] = 212;
                        imageData.data[i + 1] = 232;
                        imageData.data[i + 2] = 243;
                        imageData.data[i + 3] = 255;
                    } else {
                        imageData.data[i] = 255;
                        imageData.data[i + 1] = 255;
                        imageData.data[i + 2] = 255;
                        imageData.data[i + 3] = 255;
                    }
                }

                ctx.putImageData(imageData, 0, 0);
                resolve(getBase64Image(canvas));
            }

            image.src = source;

        });











    }

    /**
   * from: https://stackoverflow.com/questions/22172604/convert-image-url-to-base64/22172860
   * @param {Jquery} canvas 
   */
    function getBase64Image(canvas) {
        var dataURL = canvas.toDataURL("image/png");
        return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    }


    function uploadImagePromise(source) {
        return (new Promise(function (resolve, reject) {
            $.ajax({
                method: 'POST',
                url: '/upload',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    image: source
                }),
                dataType: 'json',
                success: function (e) {
                    resolve(e);
                },
                error: function (err) {
                    reject(err);
                }
            });
        }))
            .then(uploadImageSuccess)
            .catch(function (err) {
                if (err.status === 422) {
                    var message = JSON.parse(err.responseText).message;
                    alert(message);
                    $('.filter__page').show();
                    $('html,body').animate({scrollTop:0},0);
                    console.log(message);
                }
            });
    }

    function uploadImageSuccess(data) {
        console.log(data);
        var $imgTextualized = $('.js-img-textualized');

        $imgTextualized.each(function () {
            $(this)[0].src = 'data:image/png;base64,' + data.imageBase64;
        });

        $imgTextualized.attr('data-rawData', data.imageBase64);

        //put the image in the animation also
        $('.summary__page .main__container02 .img__container .img__item .image_user').attr('src', 'data:image/png;base64,' + data.imageBase64);


        $('.loader__page').hide();
        $('.filtered__page.confirm').show();
    }

});