window.LANGUAGES = {
    en: {
        'HOME': {
            INFO: 'Take a picture of your face and receive an original portrait with the design of the new Group Vision "Trusted Global Innovator"!',

            //STEP 1
            HTU_STEP1_A: 'Take your<br>face picture.',
            HTU_STEP1_B: 'Upload <br>the picture.',

            //STEP 2
            HTU_STEP2_A: 'Convert your picture into a 30th<br> anniversary design portrait.',

            //STEP 3
            HTU_STEP3_A: 'Upload your portrait on the special website.',


            VISIONARY_IMG: './assets/img/top/top_main_txt_en.svg',
            EXAMPLE_USE_IMG: './assets/img/top/pic_10_eng.png',
            TRUSTED_IMG: './assets/img/top/txt_trustedglobalinnovator_eng.png',

            TERMS_BUTTON: 'I agree to the <a href="javascript:void(0)" class="terms__link">terms of use.</a>',
            TERMS_OF_USE: 'Terms of Use',
            TERMS: '',
            STEP1_IMG: '/assets/img/top/use_container01_en.svg',
            STEP2_IMG: '/assets/img/top/use_container02_en.svg',
            STEP3_IMG: '/assets/img/top/use_container03_en.svg',
        },

        'SELECT': {
            //SELECT_PAGE
            SELECT_INNER_HEAD: 'Select a bright picture without any objects behind you.',
            SELECT_CONTENT_HEAD: 'Select a picture from chest up with your head centered in the frame.',

            //RETAKE_PAGE
            RETAKE_INFO: 'Would you like to use this picture?',

            //FILTER_PAGE
            FILTER_INFO: 'Slide the bar to adjust the contrast to show your facial parts clearly.',

            //FILTERED_PAGE
            FILTERED_INFO: 'Would you like to use this image?',

            //DETAILS_PAGE
            DETAILS_INFO: 'Please enter the following information.',

            //COMMENT_DETAILS
            DETAILS_COMMENT_HEAD: 'What does the NTT DATA vision of becoming <br>a “Trusted Global Innovator” mean to you?',
            DETAILS_COMMENT_BODY: 'Enter comments within {count} letters. (OPTIONAL)',

            //TERMS_CONTENT
            TERMS_HEAD: 'Terms of service',
            TERMS_BODY: '<strong> Mongon Atalid. Mongon Atalid. Mongon Ammon Gon Atari. Tallidus. <br> <br> Mongon Atalid. <br> Mongon Atalid. Mongon Atalid. <br> <br> </ strong> Mongon Atalid. <br> Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mon',
            TERMS_CHECK: 'I agree with the Terms & Conditions',

            //SUMMARY_PAGE
            SUMMARY_INFO: 'Would you like to register this information?',
            SUMMARY_COMMENT_HEAD: 'What is your personal motto?',

            //ARCHIVE_PAGE
            ARCHIVE_COMMENT_HEAD: 'What is your personal motto?',

            IOS_DOWNLOAD: 'Please press and hold the displayed image to save it.',
        },

        'SHOOTING': {
            //SHOOTING_PAGE
            SHOOTING_INNER_HEAD: 'Take your picture in a well-lit <br> area without any objects<br class="sp"> behind <br class="pc"> you.',
            SHOOTING_CONTENT_HEAD: 'Make sure to keep your face <br>inside the dotted line.',
            SHOOTING_CONTENT_HEAD_ALT: 'Make sure to keep your face <br>inside the dotted line.',


            //RETAKE_PAGE
            RETAKE_INFO: 'Would you like to use this picture?',

            //FILTER_PAGE
            FILTER_INFO: 'Slide the bar to adjust the contrast to show your facial parts clearly.',

            //FILTERED_PAGE
            FILTERED_INFO: 'Would you like to use this image?',

            //DETAILS_PAGE
            DETAILS_INFO: 'Please enter the following information.',

            //COMMENT_DETAILS
            DETAILS_COMMENT_HEAD: 'What does the NTT DATA vision of becoming <br>a “Trusted Global Innovator” mean to you?',
            DETAILS_COMMENT_BODY: 'Enter comments within {count} letters. (OPTIONAL)',

            //TERMS_CONTENT
            TERMS_HEAD: 'Terms of service',
            TERMS_BODY: '<strong> Mongon Atalid. Mongon Atalid. Mongon Ammon Gon Atari. Tallidus. <br> <br> Mongon Atalid. <br> Mongon Atalid. Mongon Atalid. <br> <br> </ strong> Mongon Atalid. <br> Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mongon Atalid. Mon',
            TERMS_CHECK: 'I agree with the Terms & Conditions',

            //SUMMARY_PAGE
            SUMMARY_INFO: 'Would you like to register this information?',
            SUMMARY_COMMENT_HEAD: 'What is your personal motto?',

            //ARCHIVE_PAGE
            ARCHIVE_COMMENT_HEAD: 'What is your personal motto?'
        },

        'MISC': {
            CLOSE_LABEL: 'Close',

            MODAL_SELECT_CAMERA: 'Which camera would you like to use?',
            SELECT_FRONT: 'Front Facing Camera',
            SELECT_BACK: 'Back Facing Camera',

            SELECT_WORK_LOCATION: 'WORKING LOCATION',
            SELECT_COMPANY: 'COMPANY',


            COMPANY: 'COMPANY',
            COMMENT: 'Comment',
            ACCEPT_TERMS: 'Accept the terms of service',
            ACCEPT_TERMS_ERROR: 'Terms and Conditions must be accepted.',
            CANNOT_FIND_COMPANY: 'Cannot find company',

            WORK_LOCATION_MODAL_LABEL: 'WORKING LOCATION',
            COMPANY_MODAL_LABEL: 'COMPANY',
            COMMENT_MODAL_LABEL: 'COMMENT',
            NOT_SPECIFIED: '',
            PROVIDE_ONE: 'Please provide a work location or a company name.',
            CHARACTER_COUNT: '{count} characters left.',
            NO_DATA: 'No Data',

            ATTENTION_LABEL: './assets/img/common/pic_iosattention_en.svg',
            COPY: 'Copy',
            COPIED: 'Copied',

            DETAILS_WORK_LOC_PLACEHOLDER: 'WORKING LOCATION (OPTIONAL)',
            DETAILS_COMPANY_PLACEHOLDER: 'COMPANY (OPTIONAL)',

        },

        'ERROR': {
            CAMERA_NOT_ALLOWED: 'Camera cannot be accesed.',
            SAVING_FAILED: 'An error occured while saving please try again.',
            COMMENT_LIMIT: 'Comment limit reached.',
            COMMENT_REQUIRED: 'Please provide a personal motto.',
            WORK_LOCATION_REQUIRED: 'Work Location is not found!',
            WORKLOC_COMPANY_NOT_FOUND: 'WORKING LOCATION and COMPANY is not found!',
        },

        'LOCATIONS': require('../../config/lang/en').LOCATIONS,

        'COMPANIES': require('../../config/lang/en').COMPANIES,


    },

    jp: {
        'HOME': {
            //INSTRUCTIONS
            INFO: '自分の顔を撮影して、新Group Vision <br> “Trusted Global Innovator” <br> デザインのポートレートをゲットしよう！',

            //STEP 1
            HTU_STEP1_A: '自分の顔を撮影',
            HTU_STEP1_B: '自分の顔が写った<br>写真をアップロード',

            //STEP 2
            HTU_STEP2_A: '選んだ写真を30周年オリジナルデザインへ',

            //STEP 3
            HTU_STEP3_A: '出来上がったあなたのポートレートをスペシャルサイトへアップロード',

            VISIONARY_IMG: './assets/img/top/top_main_txt.svg',
            EXAMPLE_USE_IMG: './assets/img/top/pic_10.png',
            TRUSTED_IMG: './assets/img/top/txt_trustedglobalinnovator.png',
            TERMS_BUTTON: '<a href="javascript:void(0)" class="terms__link">利用規約</a>に同意します。',
            TERMS_OF_USE: '利用規約',
            TERMS: 'モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアモンゴンアタリデス。 タリデス。<br><br> モンゴンアタリデス。 <br>モンゴンアタリデス。 モンゴンアタリデス。 <br>モンゴンアタリデス。 <br>モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 <br>モンゴンアタリデス。 モンゴンアタリデス。 <br><br>モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。<br>モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。  ',
            STEP1_IMG: '/assets/img/top/use_container01.svg',
            STEP2_IMG: '/assets/img/top/use_container02.svg',
            STEP3_IMG: '/assets/img/top/use_container03.svg',
        },

        'SELECT': {
            //SELECT_PAGE
            SELECT_INNER_HEAD: '背景に物が写っていない、明るい写真を <br> 選んでください。',
            SELECT_CONTENT_HEAD: '自分の顔が中心にくるようなバストアップ <br> の写真を選んでください。',

            //RETAKE_PAGE
            RETAKE_INFO: 'こちらの写真でよろしいですか？',

            //FILTER_PAGE
            FILTER_INFO: 'バーを左右にスライドさせ顔のパーツがわかるように <br> コントラストを調整してください。',

            //FILTERED_PAGE
            FILTERED_INFO: 'こちらのデザインでよろしいですか？',

            //DETAILS_PAGE
            DETAILS_INFO: '下記の情報を入力してください',

            //COMMENT_DETAILS
            DETAILS_COMMENT_HEAD: 'あなたにとって、NTT DATAのGroup Vision <br>“Trusted Global Innovator”とは何ですか？',
            DETAILS_COMMENT_BODY: '{count}文字以内でコメントを入力 (任意)',

            //TERMS_CONTENT
            TERMS_HEAD: '利用規約',
            TERMS_BODY: '<strong>モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアモンゴンアタリデス。 タリデス。<br><br>モンゴンアタリデス。<br>モンゴンアタリデス。 モンゴンアタリデス。<br><br></strong>モンゴンアタリデス。<br>モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モン ',
            TERMS_CHECK: '利用規約に同意する',

            //SUMMARY_PAGE
            SUMMARY_INFO: 'こちらの内容を登録しますか？',
            SUMMARY_COMMENT_HEAD: 'あなたの座右の銘を教えてください。',

            //ARCHIVE_PAGE
            ARCHIVE_COMMENT_HEAD: 'あなたの座右の銘を教えてください。',

            IOS_DOWNLOAD: '作成したポートレート画像は画像を長押しして <br> ダウンロードして下さい。',

        },

        'SHOOTING': {
            //SHOOTING_PAGE
            SHOOTING_INNER_HEAD: '明るい場所で、背景に物が映らないよう <br> 注意してください。',
            SHOOTING_CONTENT_HEAD: 'フレームの点線内に自分の顔を入れて <br> ください。',
            SHOOTING_CONTENT_HEAD_ALT: '自分の顔が中心にくるようなバストアップ<br>の写真を選んでください。',

            //RETAKE_PAGE
            RETAKE_INFO: 'こちらの写真でよろしいですか？',

            //FILTER_PAGE
            FILTER_INFO: 'バーを左右にスライドさせ顔のパーツがわかるように <br> コントラストを調整してください。',

            //FILTERED_PAGE
            FILTERED_INFO: 'こちらのデザインでよろしいですか？',

            //DETAILS_PAGE
            DETAILS_INFO: '下記の情報を入力してください',

            //COMMENT_DETAILS
            DETAILS_COMMENT_HEAD: 'あなたにとって、NTT DATAのGroup Vision <br>“Trusted Global Innovator”とは何ですか？',
            DETAILS_COMMENT_BODY: '{count}文字以内でコメントを入力 (任意)',

            //TERMS_CONTENT
            TERMS_HEAD: '利用規約',
            TERMS_BODY: '<strong>モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアモンゴンアタリデス。 タリデス。<br><br>モンゴンアタリデス。<br>モンゴンアタリデス。 モンゴンアタリデス。<br><br></strong>モンゴンアタリデス。<br>モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モンゴンアタリデス。 モン ',
            TERMS_CHECK: '利用規約に同意する必要があります',

            //SUMMARY_PAGE
            SUMMARY_INFO: 'こちらの内容を登録しますか？',

        },

        'MISC': {
            CLOSE_LABEL: '閉じる',

            MODAL_SELECT_CAMERA: 'どのカメラを使用したいですか？',
            SELECT_FRONT: 'インカメラ',
            SELECT_BACK: 'メインカメラ',

            SELECT_WORK_LOCATION: 'WORKING LOCATION',
            SELECT_COMPANY: 'COMPANY',

            COMPANY: 'COMPANY',
            COMMENT: 'COMPANY',
            ACCEPT_TERMS: '利用規約に同意する',
            ACCEPT_TERMS_ERROR: '利用規約に同意する必要があります',
            CANNOT_FIND_COMPANY: '検索されたCOMPANYが見つかりませんでした',

            WORK_LOCATION_MODAL_LABEL: 'WORKING LOCATION',
            COMPANY_MODAL_LABEL: 'COMPANY',
            COMMENT_MODAL_LABEL: 'COMMENT',
            NOT_SPECIFIED: '',
            PROVIDE_ONE: '作業場所とCOMPANYを選択してください',

            CHARACTER_COUNT: 'あと{count}文字入力できます。',
            NO_DATA: 'No Data',
            ATTENTION_LABEL: './assets/img/common/pic_iosattention.svg',
            COPY: 'コピー',
            COPIED: 'コピー済み',

            DETAILS_WORK_LOC_PLACEHOLDER: 'WORKING LOCATION (任意)',
            DETAILS_COMPANY_PLACEHOLDER: 'COMPANY (任意)',

        },

        'ERROR': {
            CAMERA_NOT_ALLOWED: 'カメラへアクセスができません。',
            SAVING_FAILED: '保存中にエラーが発生しました。 もう一度お試しください。',
            COMMENT_LIMIT: '最大文字数に達しました。',
            COMMENT_REQUIRED: '座右の銘を入力してください。',
            WORK_LOCATION_REQUIRED: '検索されたWORKING LOCATIONが見つかりませんでした。',
            WORKLOC_COMPANY_NOT_FOUND: '検索されたCOMPANYとWORKING LOCATIONが見つかりませんでした。',


        },

        'LOCATIONS': require('../../config/lang/jp').LOCATIONS,

        'COMPANIES': require('../../config/lang/jp').COMPANIES,





    }
};
