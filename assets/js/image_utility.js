window.maskCanvas;
window.$canvas;
window.$mono;
window.$binCanvas;

$('#showDbEntries').click(function(e){
    e.preventDefault();
    $.ajax({
        url: "/displayDbEntries",
        // data: ,
        type: "GET", // if you want to send data via the "data" property change this to "POST". This can be omitted otherwise
        success: function(responseData) {
        },
        error: console.error
    });
});


$("#myFile").change(function(){
    displayImg(this);
});

window.displayImg = function (input){
    if (input.files && input.files[0]) {
        var reader = new FileReader();
    
        reader.onload = function(e) {

            $('#uploadedImage').on('load', function () {
                drawCroppedImage(this);
            });

            $('#uploadedImage').attr('src', e.target.result);
        }
    
        reader.readAsDataURL(input.files[0]);
      }
}

window.drawCroppedImage = function (img)
{
    var canvasImgUploaded = document.getElementById('canvas-image_uploaded');
    var canvasImgUploadedContext = canvasImgUploaded.getContext('2d');
    var divCanvasContainer = $('#canvas-container');
    
    canvasImgUploaded.width = divCanvasContainer.width();
    canvasImgUploaded.height = divCanvasContainer.height();
    
    // canvasUploadedImg.width = img.width;
    // canvasUploadedImg.height = img.height;

    canvasImgUploadedContext.drawImage(img, 0, 0, img.width, img.height);

}

