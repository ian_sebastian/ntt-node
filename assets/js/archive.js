/**
 * archive.js
 * Client side javascript with regards to creation and retrieval
 * of all Archived users 
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @copyright Commude Philippines Inc.
 */
$(document).ready(function () {

    var stringComparisonThreshold = 0.8;
    var timeout = null;
    var companyListPromiseChecker;
    var searchboxEntriesPromiseChecker;
    var submitDetailsPromiseChecker;
    var workLocPromiseChecker;
    var allUsersPromiseChecker;

    var submitDetailsButton = $('#submitBtn');
    // submitDetailsButton.click(checkDetails);

    var workLocation = $("#workLocationSearch");
    var companySearchField = $('#companySearchList');
    var iosCompanySearchField = $('#iosCompanySelectList');


    var userComment = $('#comment');

    var workLocationArchiveSearch = $("#workLocationArchiveSearch");
    var companySearchArchive = $("#companyArchiveSearchList");
    var iosCompanySearchArchive = $(".iosCompanySelectListClass");

    var workLocationId;
    var companyId;
    var textualizedImage = $('#textualized_image');
    var imageRawData;

    var archivePage = $('.archive__page');

    companyListPromiseCall();

    if (workLocPromiseChecker !== undefined) {
        if (!workLocPromiseChecker.isPending()) {
            workLocPromiseChecker = workLocPromise();
        }
    } else {
        workLocPromiseChecker = workLocPromise();
    }

    workLocation.change(retrieveCompanies);
    // companySearchField.on("input", searchFieldFunctionality);
    // iosCompanySearchField.change(searchFieldFunctionality);

    workLocationArchiveSearch.change(retrieveCompanies);

    // companySearchArchive.on("input", searchFieldFunctionality);
    // iosCompanySearchArchive.change(retrieveCompanies);

    var confirmDetailsButton = $('#confirmBtn');
    confirmDetailsButton.click(submitDetails);

    // userComment.on("keypress", checkCommentLength);

    // function checkCommentLength(event) {
    //     if ($(userComment).val().length >= 100) {
    //         alert("Comment cannot exceed a hundred characters");
    //         event.preventDefault();
    //     }
    // }

    // if (isDeviceIOS()) {
    //     $('.autocomplete').show();
    //     $('.companySearch input, .companySearch datalist, .companyDetailsSearch input, .companyDetailsSearch datalist, .workingLocList, .workingLocList datalist').hide();
    //     $('.iosCompanySelectListClass, .iosCompanyDetailSelectListClass, .iosWorkingLocList').show();
    // }

    $(".btn__research").on('click', searchFieldFunctionality);

    function submitDetails() {
        if (submitDetailsPromiseChecker !== undefined) {
            if (!submitDetailsPromiseChecker.isPending()) {
                submitDetailsPromiseChecker = submitDetailsPromise();
            }
        } else {
            submitDetailsPromiseChecker = submitDetailsPromise();
        }

        $('.capture__page').trigger('stop_stream');

    }

    function submitDetailsPromise() {

        var currentSubmit = confirmDetailsButton.html();
        $('#counter_holder').html('');
        $('.loader__page').show();
        $('.main__container01').hide();
        imageRawData = textualizedImage.attr('data-rawdata');
        $('html,body').animate({scrollTop:0},0);
        var custom_company;


        // if (isDeviceIOS()) {
        //     workLocationId = '000';
        //     var workLocationIdTemp = $('#workingLocationIdSelected').val();
        //     if (workLocationIdTemp !== "") {
        //         workLocationId = workLocationIdTemp;
        //     }
        //     companyId = $('#idSelected').val();

        //     custom_company = $('#iosDetailsCompanySelect').val();

        // } else {
        //     custom_company = $('#companySearchList').val();
        // }

        custom_company = $('#companySearchList').val();

        return (new Promise(function (resolve, reject) {
            $.ajax({
                method: 'POST',
                url: '/saveUserDetails',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    image_rawdata: imageRawData,
                    work_location_id: workLocationId,
                    // company_name: $('table #company_summary').text(),
                    company_id: companyId,
                    company_custom: custom_company,
                    comment: $('#comment_summary').text(),
                    original: originalImage.replace(/^data:image\/(png|jpg);base64,/, ""),
                    original_flip: originalImageFlip
                }),
                dataType: 'json',
                success: function (e) {
                    resolve(e);
                },
                error: function (err) {
                    reject(err);
                }
            });
        })).then(function () {
            console.log("Inserted in database");
            $('.btn__summary').trigger('startAnimation');
            // companyListPromiseCall("000"); //orig
        }).catch(function (err) {
            console.log(err);
            if (err.status === 422) {
                var message = JSON.parse(err.responseText).message;
                alert(message);
                confirmDetailsButton.html(currentSubmit);
            }
        })
    }

    /**
     * For Work Location drop down list
     * Performs:
     *  Preparation of company searchbox
     *  Displays all users based on selected work location
     */
    function retrieveCompanies() {
        //run if details_page is shown
        if ($('.details__page:visible').length != 0) {
            // companySearchField.removeAttr("disabled");

            // if (workLocation.val() == "000") {
            //     companySearchField.prop("disabled", true);
            // }
            $('.iosCompanySelectListClass').val('');

            companySearchField.val("");
            $idHidden.val('');

            // companyListPromiseCall(workLocation.val());
        } else {
            //run if archive_page is shown
            // companySearchArchive.removeAttr("disabled");

            // if (workLocationArchiveSearch.val() == "000") {
            //     companySearchArchive.prop("disabled", true);
            // }

            companySearchArchive.val("");
            $idHidden.val('');

            // companyListPromiseCall(workLocationArchiveSearch.val());
        }

    }

    function companyListPromiseCall() {
        if ($('.details__page:visible').length != 0) {
            if (companyListPromiseChecker !== undefined) {
                if (!companyListPromiseChecker.isPending()) {
                    companyListPromiseChecker = userCompanyDetailListPromise();
                }
            } else {
                companyListPromiseChecker = userCompanyDetailListPromise();
            }
        } else {
            if (companyListPromiseChecker !== undefined) {
                if (!companyListPromiseChecker.isPending()) {
                    companyListPromiseChecker = companyListPromise();
                }
            } else {
                companyListPromiseChecker = companyListPromise();
            }
        }
    }

    $('.iosCompanySelectListClass').on('locale_update', function () {
        //from autocomplete.js file

        if (companyListPromiseChecker !== undefined) {
            if (!companyListPromiseChecker.isPending()) {
                companyListPromiseChecker = companyListPromise();
            }
        } else {
            companyListPromiseChecker = companyListPromise();
        }


        if (workLocPromiseChecker !== undefined) {
            if (!workLocPromiseChecker.isPending()) {
                workLocPromiseChecker = workLocPromise();
            }
        } else {
            workLocPromiseChecker = workLocPromise();
        }
    });


    function companyListPromise(workocVal) {

        return (new Promise(function (resolve, reject) {
            $.ajax({
                method: 'POST',
                url: '/retrieveCompanyEntries',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                }),
                dataType: 'json',
                success: function (e) {
                    resolve(e);
                },
                error: function (err) {
                    reject(err);
                }
            });
        })).then(initializeAutocomplete)
            .catch(function (err) {
                console.log('From Company List Promise', err);
            });
    }


    function workLocPromise(workocVal) {

        return (new Promise(function (resolve, reject) {
            $.ajax({
                method: 'POST',
                url: '/retriveAllWorkLoc',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                }),
                dataType: 'json',
                success: function (e) {
                    resolve(e);
                },
                error: function (err) {
                    reject(err);
                }
            });
        })).then(initializeAutocompleteWorkLoc)
            .catch(function (err) {
                console.log('From Company List Promise', err);
            });
    }



    function allUsersPromise() {

        return (new Promise(function (resolve, reject) {
            $.ajax({
                method: 'POST',
                url: '/allUsers',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                }),
                dataType: 'json',
                success: function (e) {
                    resolve(e);
                },
                error: function (err) {
                    reject(err);
                }
            });
        })).then(archiveInitialized)
            .catch(function (err) {
                console.log('From Company List Promise', err);
            });
    }

    function archiveInitialized(data) {
        displaySearchImageResults(data);

        $('.loader__page').fadeOut(1500, function () {
            $('.archive__page').fadeIn(4000);
            $('footer').show();
        });
    }

    function initializeAutocompleteWorkLoc(data) {

        $workLocHidden.val('');
        var workLocList = [];
        var workLocListWithId = {};

        $.each(data.list, function (key, val) {
            workLocList.push(LANGUAGES[currentLocale].LOCATIONS[val]);
            workLocListWithId[LANGUAGES[currentLocale].LOCATIONS[val]] = key;
        });
        autocomplete($('#iosWorkingLocationSelect')[0], workLocList, workLocListWithId, '#workingLocationIdSelected');
        if ($('#iosWorkingLocationSelectDetails').length > 0) {
            autocomplete($('#iosWorkingLocationSelectDetails')[0], workLocList, workLocListWithId, '#workingLocationIdSelected');
        }
    }


    function initializeAutocomplete(data) {
        // alert(JSON.stringify(data.companyListWithId));

        var newList = [];
        var newListWithId = {};

        data.list.forEach(function (val) {
            newList.push(LANGUAGES[currentLocale].COMPANIES[val]);
        });

        $.each(data.companyListWithId, function (key, val) {
            newListWithId[LANGUAGES[currentLocale].COMPANIES[key]] = val;
        });

        autocomplete($('#iosCompanySelect')[0], newList, newListWithId, '#idSelected');
        if ($('#iosDetailsCompanySelect').length > 0) {
            autocomplete($('#iosDetailsCompanySelect')[0], newList, newListWithId, '#idSelected');
        }
    }

    function userCompanyDetailListPromise(workLocVal) {

        return (new Promise(function (resolve, reject) {
            $.ajax({
                method: 'POST',
                url: '/retrieveUserDetailCompanyEntries',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    work_location_id: workLocVal
                }),
                dataType: 'json',
                success: function (e) {
                    resolve(e);
                },
                error: function (err) {
                    reject(err);
                }
            });
        })).then(selectSearchFunctionality)
            .catch(function (err) {
                console.log('From Company List Promise', err);
            });
    }


    function selectSearchFunctionality(data) {
        if (!$("#archive_page").hasClass("page_load")) {
            prepareCompanyDatalist(data);
        }

        if ($('.details__page:visible').length != 0) {
            prepareCompanyDetailDatalist(data);
        } else if ($('.home:visible').length == 0) {
            // companyListPromiseCall("000");
            $('.loader__page').fadeOut(1500, function () {
                $('.archive__page').fadeIn(3000);
                $('footer').show();
            });

            if ($("#archive_page").hasClass("page_load")) {
                displaySearchImageResults(data);
                $("#archive_page").removeClass("page_load");
            }

        } else {
            // alert("DITO");
            prepareCompanyDatalist(data);
        }
    }

    function displaySearchImageResults(data) {
        if (data.users.length === 0) {
            isReady = false;
            $('.archive__list').html(`<p class="searchResults">${LANGUAGES[currentLocale].MISC.NO_DATA}</p>`);
        } else {
            isReady = true;
            if ($('.details__page:visible').length == 0) {
                var userSearchResults = "<div class=\"allUsers\">";
                data.users.forEach(function (element) {
                    var companyName = LANGUAGES[currentLocale].COMPANIES[element.company_name];
                    var workLoc = LANGUAGES[currentLocale].LOCATIONS[element.work_location_name];
                    var comment = element.user_comment1;

                    if (element.company_name === undefined || element.company_name === null || element.company_name === '') {
                        companyName = LANGUAGES[currentLocale].MISC.NOT_SPECIFIED;
                        if (element.custom_company !== null) {
                            companyName = element.custom_company;
                        }
                    }

                    if (element.work_location_name === undefined || element.work_location_name === null || element.work_location_name === '') {
                        workLoc = LANGUAGES[currentLocale].MISC.NOT_SPECIFIED;

                    }

                    if (comment === null) {
                        comment = "";
                    }


                    userSearchResults += "<div class=\"archive__items reveal\">";
                    userSearchResults += '<img id="image_' + element.user_id + '" src="./images/' + element.user_image_filepath + '" alt="" data-userImageFilePath="./images/'
                        + element.user_image_filepath + '" data-userComment="' + comment + '" data-companyName="' + companyName
                        + '" data-workLocation="' + workLoc + '" class="image_appended ' + element.company_name + ' ' + element.work_location_name + '">';

                    userSearchResults += "</div>";

                });
                userSearchResults += "</div>";
                $(".archive__list").html(userSearchResults);
                $(document).trigger('image_nav_rebind_event');

                var images = $('.image_appended');
                for (let i = 0; i < images.length; i++) {
                    const element = images[i];
                    element.onload = fadeInImages;
                }

                toggleModal();
            }
        }

    }

    function prepareCompanyDetailDatalist(data) {
        var searchResults = "";
        if (!isDeviceIOS()) {
            data.companies.forEach(function (element) {
                searchResults += "<option value=" + element.company_name + ">" + element.company_name + " </option>";
            });
            document.getElementById("companyDatalist").innerHTML = searchResults;
        } else {
            // searchResults = '<select id="iosCompanyDetailSelect" class="btn__list iosCompanyDetailSelectListClass">';
            // searchResults += '<option value="default">' + LANGUAGES[currentLocale].MISC.COMPANY + '</option>';
            // data.companies.forEach(element => {
            //     var optionBuilder = {
            //         company_name: element.company_name,
            //         work_location: element.work_location_name
            //     }
            //     // searchResults += "<option value=" + element.company_name + ">" + element.company_name + " </option>";
            //     searchResults += '<option value="' + element.company_name + '" data-value=\'' + JSON.stringify(optionBuilder) + '\'>' + element.company_name + " </option>";
            // });
            // searchResults += "</select>";

            // $(".companyDetailsSearch").html(searchResults);

        }



    }

    function prepareCompanyDatalist(data) {
        if (!isDeviceIOS()) {
            var searchResults = "";
            data.companies.forEach(function (element) {
                var optionBuilder = {
                    company_name: element.company_name,
                    work_location: element.work_location_name
                }
                // searchResults += "<option value='" + element.company_name + " " + element.work_location_name + "'data-value='{\"company_name\":'" + element.company_name +"',\"work_location\":'" + element.work_location_name +"'}'>" + element.company_name + " " + element.work_location_name + " </option>";
                searchResults += '<option value="' + element.company_name + ' ' + element.work_location_name + '" data-value=\'' + JSON.stringify(optionBuilder) + '\'>' + element.company_name + " " + element.work_location_name + " </option>";
            });
            document.getElementById("companyDatalist").innerHTML = searchResults;

        } else {
            var searchResults = '<select id="iosCompanySelect" class="btn__list iosCompanySelectListClass">';
            searchResults += '<option value="default">' + LANGUAGES[currentLocale].MISC.COMPANY + '</option>';
            data.companies.forEach(function (element) {
                var optionBuilder = {
                    company_name: element.company_name,
                    work_location: element.work_location_name
                }
                // searchResults += "<option value=" + element.company_name + ">" + element.company_name + " </option>";
                searchResults += '<option value="' + element.company_name + '" data-value=\'' + JSON.stringify(optionBuilder) + '\'>' + element.company_name + " " + element.work_location_name + " </option>";
            });
            // alert("WIWOW");
            searchResults += "</select>";

            $(".companySearch").html(searchResults);

            //run if archive__page is shown
            // if ($('.archive__page:visible').length != 0) {
            //     $('#iosCompanySelect').attr('disabled', true);
            // }
            iosCompanySearchArchive = $(".iosCompanySelectListClass");
            // $(".iosCompanySelectListClass").on("change", function(){
            //     $('#iosCompanySelect').change(retrieveCompanies);
            // })
            $(".btn__research").on('click', searchFieldFunctionality);
        }

    }

    /**
    * For companySearchField and companySearchArchive searchbox 
    * Performs:
    *  Displays all users based on selected work location and searchbox entry
    *  Filters the users based on searchbox entry in realtime
    */
    function searchFieldFunctionality(e) {
        var company;
        var option;

        option = $("#companyDatalist").find("[name='" + $("#companyArchiveSearchList").val() + "']");
        companyId = option.attr('id');

        workLocationId = '000';


        var work_location_value = $('#workingLocationSearchList').val();
        if (work_location_value !== "") {
            var temp = $(`option[name="${work_location_value}"]`).attr('id');
            if (temp !== undefined) {
                workLocationId = temp;
            }
        }

        $('.archive__list').html('');

        searchUsers();

    }

    function searchUsers(company) {
        if (searchboxEntriesPromiseChecker !== undefined) {
            if (!searchboxEntriesPromiseChecker.isPending()) {
                searchboxEntriesPromiseChecker = searchEntriesPromise(company);
            }
        } else {
            searchboxEntriesPromiseChecker = searchEntriesPromise(company);
        }
    }

    function searchEntriesPromise(company) {

        var custom_company;

        // if (isDeviceIOS()) {
        //     custom_company = $('#iosCompanySelect').val();

        // } else {
        //     custom_company = $('#companyArchiveSearchList').val();
        // }

        custom_company = $('#companyArchiveSearchList').val();

        // alert(company.work_location);
        return (new Promise(function (resolve, reject) {
            $.ajax({
                method: 'POST',
                url: '/searchDbEntries',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    work_location_id: workLocationId,
                    // work_location_id: workLocationArchiveSearch.val(),
                    company_id: companyId,
                    custom: custom_company
                    // work_location_name: company.work_location,
                    // company_name: company.company_name
                }),
                dataType: 'json',
                success: function (e) {
                    resolve(e);
                },
                error: function (err) {
                    reject(err);
                }
            });
        })).then(displayCompanyResult)
            .catch(function (err) {
                console.log('From search entries promise', err);
            });

    }

    function displayCompanyResult(data) {
        displaySearchImageResults(data);
    }

    var testDiv = document.getElementById("testDiv");
    var testDiv2 = document.getElementById("testDiv2");
    var backBtn = document.getElementById("btn_back");

    function hideVideo() {
        document.getElementById('testDiv').classList.toggle('hidden');
    }

    function showVideo() {
        document.getElementById('testDiv2').classList.toggle('hidden');
    }

    $('#uploadImage').click(function () {
        $(".uploadImageContainer").toggle();
        $(".mainModules").toggle();
        $(".initialButtons").toggle();
    })

    $('#takeImage').click(function () {
        $(".takeImageContainer").toggle();
        $(".mainModules").toggle();
        $(".initialButtons").toggle();
    })

    $('#showDbEntries').click(function () {
        $(".initialButtons").toggle();
        $(".databaseEntries").toggle();
    })

    $('.js--btn__summary').on('click', function (event) {
        var datalistObject;
        var work_location;
        var $companySearchList = $('#companySearchList');
        var $workingLocationSaveList = $('#workingLocationSaveList');

        var companyCheck = false;
        var workLocationCheck = false;

        $('#company_summary').html('');
        $('#work_location_summary').html('');

        var locations = _.values(LANGUAGES[currentLocale].LOCATIONS);
        var companies = _.values(LANGUAGES[currentLocale].COMPANIES);
        //make comparisons for the COMPANY INPUT
        var companySearchValue = $companySearchList.val();
        if (!_.isEmpty(companySearchValue)) {
            var threshold = 0.8;
            var checkNTT = CompareString.compareTwoStrings(companySearchValue, 'nttdata');
            var checkOptimal = CompareString.compareTwoStrings(companySearchValue, 'Optimal Solutions');

            if (checkNTT > threshold) {
                // datalistObject = $("#companyDatalist").find("option[name='NTT DATA, Inc.']");
                // $('#company_summary').html('NTT DATA, Inc.');
                // companyId = datalistObject.attr('id');
                companyCheck = true;
            } else if (checkOptimal > threshold) {
                datalistObject = $("#companyDatalist").find("option[name='Optimal Solutions Integration (Software) Bd. Pvt. Ltd.']");
                $('#company_summary').html('Optimal Solutions Integration (Software) Bd. Pvt. Ltd.');
                companyId = datalistObject.attr('id');
            } else {
                var result = CompareString.findBestMatch(companySearchValue, companies);
                //check if the best match passes throught the threshold
                if (result.bestMatch.rating > stringComparisonThreshold) {
                    datalistObject = $("#companyDatalist").find("option[name='" + result.bestMatch.target + "']");
                    $('#company_summary').html(result.bestMatch.target);
                    companyId = datalistObject.attr('id');

                } else {
                    // alert(LANGUAGES[currentLocale].MISC.CANNOT_FIND_COMPANY);
                    // return false;
                    companyCheck = true;
                }
            }
        }

        workLocationId = '000';
        //make comparisons for the WORKING LOCATION INPUT
        var workingLocationValue = $workingLocationSaveList.val();
        if (!_.isEmpty(workingLocationValue)) {
            var result = CompareString.findBestMatch(workingLocationValue, locations);
            if (result.bestMatch.rating > stringComparisonThreshold) {
                work_location = $("#workingLocationListSave").find("option[name='" + result.bestMatch.target + "']");
                $('#work_location_summary').html(result.bestMatch.target);
                workLocationId = work_location.attr('id');

            } else {
                // alert(LANGUAGES[currentLocale].ERROR.WORK_LOCATION_REQUIRED);
                // return false;
                workLocationCheck = true;
            }

        }

        if (companyCheck && workLocationCheck) {
            alert(LANGUAGES[currentLocale].ERROR.WORKLOC_COMPANY_NOT_FOUND);
            return false;

        } else if (companyCheck && !workLocationCheck) {
            alert(LANGUAGES[currentLocale].MISC.CANNOT_FIND_COMPANY);
            return false;
        } else if (!companyCheck && workLocationCheck) {
            alert(LANGUAGES[currentLocale].ERROR.WORK_LOCATION_REQUIRED);
            return false;
        }


        if ($('#comment').val().replace(/\s/g, "") === "") {
            // alert(LANGUAGES[currentLocale].ERROR.COMMENT_REQUIRED);
            // return false;

            $('#SHOOTING_DETAILS_COMMENT_HEAD').hide();
        } else {
            $('#SHOOTING_DETAILS_COMMENT_HEAD').show();

        }


        if ($('#details_checkbox').prop('checked') == false) {
            alert(LANGUAGES[currentLocale].MISC.ACCEPT_TERMS_ERROR);
            return false;
        }

        $('#comment_summary').html($('#comment').val());
        $('.details__page').hide();
        $('.summary__page').show();
        $('html,body').animate({scrollTop:0},0);
    });

    archivePage.on('animationFinish', function () {
        $idHidden.val('');
        $workLocHidden.val('');
        allUsersPromise();
    });

})