$(function () {

    var commentLimit = {
        en: 1000,
        jp: 500,
    };

    var characterCount = $('#characterCount');
    characterCount.html(replaceText(LANGUAGES[currentLocale].MISC.CHARACTER_COUNT, {count: commentLimit[currentLocale]}));

    var checkComment = function(e) {
        var value = $(this).val();
        var left = commentLimit[currentLocale] - value.length;

        if (left < 0) {
            left = 0;
        }


        if (value.length > commentLimit[currentLocale]) {
            $(this).val(value.slice(0, commentLimit[currentLocale]));
            alert(LANGUAGES[currentLocale].ERROR.COMMENT_LIMIT);
            e.preventDefault();
            characterCount.html(replaceText(LANGUAGES[currentLocale].MISC.CHARACTER_COUNT, {count: left}));

            return;
        }

        characterCount.html(replaceText(LANGUAGES[currentLocale].MISC.CHARACTER_COUNT, {count: left}));


    }

    

    $('#comment').keyup(checkComment);

    $('#comment').on('paste', function(e) {
        setTimeout(() => {
            checkComment = checkComment.bind(this);
            checkComment(e);
        }, 0);
    });


    
});