window.isIE = false;

// scroll effect on anchor tag to id
window.scroll = function () {
	$('a[href^="#"]').click(function () {
		var href = $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$('html, body').stop().animate({ scrollTop: position }, 500, 'swing');
		return false;
	});
}

window.checkbox = function () {
	$('.terms__container').on('click', function () {
		var checkbox1 = $('.terms__container .termsandcondition');
		checkbox1.prop("checked", !checkbox1.prop("checked"));
		$('.button__container').toggleClass('js--terms');
	});

	$('.home .btn__camera, .home .btn__upload').on('click', function () {
		var checkbox1 = $('.terms__container .termsandcondition');
		checkbox1.prop("checked", !checkbox1.prop("checked"));
		$('.button__container').toggleClass('js--terms');
	});
}

window.copyText = function () {
	$('.btn--copy').on('click', function () {
		$('.copy__item').removeClass('js--copied');
		$('.btn--copy').html(LANGUAGES[currentLocale].MISC.COPY);
		$(this).html(LANGUAGES[currentLocale].MISC.COPIED);
		$(this).prev('input').select();
		$(this).parent('.copy__item').addClass('js--copied');
		document.execCommand("copy");
		iosCopyToClipboard($(this).prev('input')[0]);
	})
}

function iosCopyToClipboard(el) {
	var oldContentEditable = el.contentEditable,
		oldReadOnly = el.readOnly,
		range = document.createRange();

	el.contenteditable = true;
	el.readonly = false;
	range.selectNodeContents(el);

	var s = window.getSelection();
	s.removeAllRanges();
	s.addRange(range);

	el.setSelectionRange(0, 999999); // A big number, to cover anything that could be inside the element.

	el.contentEditable = oldContentEditable;
	el.readOnly = oldReadOnly;

	document.execCommand('copy');
}

function sampleCameraCapture() {
	// Grab elements, create settings, etc.
	var video = document.getElementById('video');

	// Get access to the camera!
	if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
		// Not adding `{ audio: true }` since we only want video now
		navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
			video.src = window.URL.createObjectURL(stream);
			video.play();
		});
	} else if (navigator.getUserMedia) { // Standard
		navigator.getUserMedia({ video: true }, function (stream) {
			video.src = stream;
			video.play();
		}, errBack);
	} else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
		navigator.webkitGetUserMedia({ video: true }, function (stream) {
			video.src = window.webkitURL.createObjectURL(stream);
			video.play();
		}, errBack);
	} else if (navigator.mozGetUserMedia) { // Mozilla-prefixed
		navigator.mozGetUserMedia({ video: true }, function (stream) {
			video.src = window.URL.createObjectURL(stream);
			video.play();
		}, errBack);
	}
}

window.toggleModal = function ($e) {
	$('.archive__items').on('click', function () {
		// alert("Sokpa");
		var archiveItemData = $(this).find($('img'));
		var userImage = archiveItemData[0].dataset.userimagefilepath;
		var workLocation = archiveItemData[0].dataset.worklocation;
		var companyName = archiveItemData[0].dataset.companyname;
		var userComment = archiveItemData[0].dataset.usercomment;

		$('#img-userImage').attr('src', userImage);
		$('#td-workLocationModal').html(workLocation);
		$('#td-companyModal').html(companyName);
		$('#span-userComment').html(userComment);
		// alert("Sokpa");
		$('.archive__modal').fadeIn();
		$('.main__container').addClass('modal-open');
		$('body').addClass('modal__open');

		if (isDeviceIOS()) {
			//$('body').addClass('iphone__fix');
		}

	});

	$('.notif__modal .btn__okay').on('click', function () {
		$('.notif__modal').removeClass('show');
		$('body').removeClass('iphone__fix');
		$('body').removeClass('modal__open');
	});

	$('.btn__close').on('click', function () {
		// $(this).parent().fadeOut();
		$('.modal__container').fadeOut();
		$('.main__container').removeClass('modal-open');
		$('body').removeClass('modal__open');
		if (isDeviceIOS()) {
			//$('body').removeClass('iphone__fix');
		}

	});


	// $('#shootOk__btn').on('click', function () {

	// 	// if (detectmob()) {
	// 	// 	$('.modal__container').fadeIn();
	// 	// 	// alert(detectmob());
	// 	// } else {
	// 	// 	alert("HELLO");
	// 	// 	$('.capture__page').show();
	// 	// }
	// });

	// $('#select__retake__btn').on('click', function () {
	// 	// if ($("#upload__selectedFacingMode").attr('data-selectedFacingMode') == "") {
	// 	// 	$('.modal__container').fadeIn();
	// 	// } else {
	// 	// 	$('.capture__page').show();
	// 	// }
	// });
}

// function animationNTT() {
// 	$('.btn__summary').on('click', function() {

// 	});
// }

window.detectmob = function () {
	if (navigator.userAgent.match(/Android/i)
		|| navigator.userAgent.match(/webOS/i)
		|| navigator.userAgent.match(/iPhone/i)
		|| navigator.userAgent.match(/iPad/i)
		|| navigator.userAgent.match(/iPod/i)
		|| navigator.userAgent.match(/BlackBerry/i)
		|| navigator.userAgent.match(/Windows Phone/i)
	) {
		return true;
	}
	else {
		return false;
	}
}

// function isMobileDeviceTest() {
// 	var check = false;
// 	(function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
// 	return check;
// };

// function isMobileDevice() {
// 	return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
// };


$('.btn__summary').on('startAnimation', function () {
	// $('.loader__page').hide();
	$('footer').hide();

	$('.loader__page').fadeOut(1500, function () {
		/*$('.main__container01').fadeOut(1500, function () {
			setTimeout(function () {
				$('.main__container02').hide().fadeIn('100', function () {
					setTimeout(function () {
						$('.main__container02 .img__container').addClass("reverse");
						setTimeout(function () {
							$('.main__container03').hide().fadeIn(1300, function () {
								setTimeout(function () {
									$('.main__container02').fadeOut('fast');
									setTimeout(function () {
										//s$('.main__container03 .bg__container').addClass("slideLeft");
										//setTimeout(function () {
											$('.summary__page').fadeOut(100, function () {
												$('.archive__page').hide();
												$('footer').show();
												$('.loader__page').fadeIn(2000);
												// $('.archive__page').fadeIn(2000); //orig
												$('.archive__page').trigger('animationFinish');
												// companyListPromiseCall("000");
											});
										//}, 4500);
									}, 4500);
								}, 300);
							});
						}, 1500);
					}, 4000);
				});
			}, 500);
		});*/

		$('.main__container03').velocity({
			opacity: '1',
			// display: 'block'
			// visibility: 'visible',
			// backgroundColor: '#000000',
			// translateX: '+=200px',
			// translateY: '+=200px'
		}, {
				// Option
				duration: 1300, // アニメーション時間
				easing: 'ease-in-out', // イージング : linear, swing, ease, ease-in, ease-out, ease-in-out, [200, 15]
				begin: function () {
					// console.log('start');
				}, // or null
				progress: null, // 進捗率
				complete: function () {
					// console.log('end');
				}, // or null
				loop: false, // 繰り返し : or false
				delay: 5100, // 開始、ループ時に遅延させる Ex.1000
				display: 'block' // 表示・非表示 : false, 'none', 'block'
			});

		$('.main__container03 .bg__container').velocity({
			// scaleX: 1,
			// scaleY: 1
		}, {
				// Option
				duration: 1000, // アニメーション時間
				easing: 'ease-in-out',
				progress: null, // 進捗率
				loop: false, // 繰り返し : or false
				delay: 5100, // 開始、ループ時に遅延させる Ex.1000
			});

		$('.main__container01').removeClass('js-animate');
		//$('.main__container03').addClass('js-animate');
		setTimeout(function () { // 500
			$('.main__container02').show();
			setTimeout(function () {
				$('.main__container02').addClass('js-animate');
				setTimeout(function () { // 3000
					$('.main__container02 .img__container').addClass('reverse');
					setTimeout(function () { // 1600
						$('.main__container03').addClass('js-animate');
						setTimeout(function () { // 300
							// $('.main__container02').removeClass('js-animate');
							setTimeout(function () { // 4500
								// $('.loader__page').fadeIn(2000);
								$('.summary__page').fadeOut(100, function () {
									$('.archive__page').hide();
									$('footer').show();
									$('.loader__page').fadeIn(2000);
									// $('.archive__page').fadeIn(2000); //orig
									$('.archive__page').trigger('animationFinish');
									// companyListPromiseCall("000");
								});
							}, 4500);
						}, 300);
					}, 1600);
				}, 3000);
			}, 100);
		}, 500);
	});
});

window.counter = function () {
	// counter
	$('.counter').each(function () {
		var $this = $(this),
			countTo = $this.attr('data-count');

		$({ countNum: $this.text() }).animate({ countNum: countTo }, {
			duration: 4900,
			easing: 'linear',
			step: function () {
				$this.text(Math.floor(this.countNum));
			},
			complete: function () { $this.text(this.countNum); }
		});
	});
}

window.toggleNextPage = function () {
	// select 
	$('.js--btn__select').on('click', function () {

		// 2018/05/16 Louie: Move to file on change event -> select_page.js
		// $('.select__page').hide();
		// $('.retake__page').show();
	});

	// shooting
	// $('.js--btn__shooting').on('click', function () {
	// 	$('.shooting__page').hide();
	// 	$('.capture__page').show();
	// });

	// capture page
	$('.js--btn__capture').on('click', function () {
		// $('.capture__page').hide();
		// $('.retake__page').show();
		$(window).scrollTop(0);

	});

	// retake page
	$('.js--bck__capture').on('click', function () {
		$('.retake__page').hide();
		$('.capture__page').show(0, function () {
			jQuery('html,body').animate({ scrollTop: 0 }, 0);
		});
		// computeCameraContainer();
		$(window).scrollTop(0);

		$('#img-temporary').attr('src', ''); // Clear Image
	});

	$('.js--bck__capture_upload').on('click', function () {

		$(window).scrollTop(0);

		// $('#img-temporary').attr('src', ''); // Clear Image
	});

	//capture page
	$('.js--btnbckcapture').on('click', function () {
		$('.capture__page').hide();
		$('.shooting__page').show();
	});

	// retake page
	$('.js--bck__upload').on('click', function () {
		$('.retake__page').hide();
		$('.select__page').show(); //same class but different .hbs file
		$(window).scrollTop(0);

		$('#img-temporary').attr('src', ''); // Clear Image
	});

	$('.js--btn__filter').on('click', function () {
		$('#hdn-video_width').val($('#img-temporary').width());
		$('#hdn-video_height').val($('#img-temporary').height());
		$(window).scrollTop(0);

		// $('.retake__page').hide();
		// $('.filter__page').show();
	});

	// filter page
	$('.js--bck__retake').on('click', function () {
		$('.filter__page').hide();
		$('.retake__page').show();
		$(window).scrollTop(0);

	});
	$('.js--btn__filteredConfirm').on('click', function () {
		$('.filter__page').hide();
		$('.counter').text('0');
		$(window).scrollTop(0);
	});

	// filtered page
	$('.js--bck__filter').on('click', function () {
		$('.filtered__page').hide();
		// $('.retake__page').show();
		$('.filter__page').show();
		$(window).scrollTop(0);

	});
	$('.js--btn__details').on('click', function () {
		$('.filtered__page').hide();
		$('.details__page').show();
		$(window).scrollTop(0);
	});

	$('.js--bck__filteredDownload').click(function () {
		$('.details__page').hide();
		$('.filtered__page.download').show();
		$(window).scrollTop(0);

	});

	$('.js--bck__filteredConfirm').click(function () {
		$('.filtered__page.download').hide();
		$('.filtered__page.confirm').show();
		$(window).scrollTop(0);

	});


	$('.js--btn__filteredDownload').click(function () {
		$('.filtered__page.confirm').hide();
		$('.filtered__page.download').show();
		$(window).scrollTop(0);


	});

	// details page
	$('.js--bck__filtered').on('click', function () {
		$('.details__page').hide();
		$('.filtered__page').show();
		$(window).scrollTop(0);

	});


	// summary page
	$('.js--bck__details').on('click', function () {
		$('.summary__page').hide();
		$('.details__page').show();
		$(window).scrollTop(0);

	});


	$('.btn__research').click(function () {
		ctr = 0;
		isReady = true;
	});
}

window.toggleAccordion = function () {
	$('.mainUse__container .container__header').on('click', function () {
		// $('.mainUse__container').toggleClass('js-accordion');
		// $('.mainUse__container .content__container').slideToggle();

		if ($(window).width() < 768) {
			$('.mainUse__container').toggleClass('js-accordion');
			$('.mainUse__container .content__container').slideToggle();
		}

	});
}

// function toggleCompanyNameSearchField(){
// 	$('#companySearchList').removeAttr("disabled");
// }

window.ctr = 0;
window.workLocationArchiveSearch = $("#workLocationArchiveSearch");
window.companySearchArchive = $("#companyArchiveSearchList");
window.scrollResultPromiseChecker = undefined;

// workLocationArchiveSearch.change(resetCounter);
// companySearchArchive.on("input", resetCounter);
// $('#iosCompanySelect').on("change", resetCounter);

window.resetCounter = function () {
	// alert("reset");
	$(".archive__list").scrollTop(0);
	ctr = 0;
	isReady = true;
}

window.isReady = true;

$(window).on('scroll', function () {

	if (($(window).scrollTop() + $(window).height() >= ($(document).height() - 100)) && $('.archive__page').css('display') == 'block' && isReady) {
		ctr++;
		isReady = false;
		retrieveScrollResults();
	}

	if (($(window).scrollTop() + $(window).height() >= ($(document).height() - 100)) && $('.home').css('display') == 'block' && isReady) {
		ctr++;
		isReady = false;
		retrieveScrollResults();
	}

}); // scroll


window.retrieveScrollResults = function () {
	if (scrollResultPromiseChecker !== undefined) {
		if (!scrollResultPromiseChecker.isPending()) {
			var option = $("#companyDatalist").find("[name='" + $("#companyArchiveSearchList").val() + "']");
			scrollResultPromiseChecker = retrieveScrollResultsPromise(workLocationArchiveSearch, option.attr('id'));

		}
	} else {
		var option = $("#companyDatalist").find("[name='" + $("#companyArchiveSearchList").val() + "']");
		scrollResultPromiseChecker = retrieveScrollResultsPromise(workLocationArchiveSearch, option.attr('id'));
	}
}

window.retrieveScrollResultsPromise = function (workLocation, companyName) {
	var compId;
	if (companyName !== undefined) {
		compId = typeof companyName === 'string'
			? companyName
			: companyName.val();
	}

	var work_location_id = '000';

	var work_location_value = $('#workingLocationSearchList').val();

	if (work_location_value !== "") {
		var temp = $(`option[name='${work_location_value}']`).attr('id');
		if (temp !== undefined) {
			work_location_id = temp;
		}
	}

	var custom_company;

	custom_company = $('#companyArchiveSearchList').val();



	return (new Promise(function (resolve, reject) {
		$.ajax({
			method: 'POST',
			url: '/retrieveScrollResults',
			contentType: 'application/json; charset=utf-8',
			data: JSON.stringify({
				work_location_id: work_location_id,
				// work_location_id: workLocation.val(),
				// company_name: companyName.val(),
				company_id: compId,
				counter: ctr,
				custom: custom_company
			}),
			dataType: 'json',
			success: function (e) {
				resolve(e);
			},
			error: function (err) {
				alert("error");
				reject(err);
			}
		});
	})).then(displayScrollResults)
		.catch(function (err) {
			if (err.status === 422) {
				var message = JSON.parse(err.responseText).message;
				alert(message);
			}
		});
}

window.replaceText = function (orig, str_var) {
	if (typeof orig === 'string') {
		var val = orig;
		$.each(str_var, function (key, value) {
			val = val.replace(new RegExp('{' + key + '}', 'g'), value);
		});
		return val;
	} else {
		return orig;
	}
}


window.displayScrollResults = function (data) {
	if (data.length !== 0) {
		// alert("pasokk");

		$('.loader__container').fadeIn();
		setTimeout(function () {

			// var appendCtr = 9;
			var userSearchResults = "";
			data.forEach(function (element) {
				var companyName = LANGUAGES[currentLocale].COMPANIES[element.company_name];
				var workLoc = LANGUAGES[currentLocale].LOCATIONS[element.work_location_name];
				var comment = element.user_comment1;


				if (element.company_name === undefined || element.company_name === null || element.company_name === '') {
					companyName = LANGUAGES[currentLocale].MISC.NOT_SPECIFIED;
					if (element.custom_company !== null) {
						companyName = element.custom_company;
					}
				}

				if (element.work_location_name === undefined || element.work_location_name === null || element.work_location_name === '') {
					workLoc = LANGUAGES[currentLocale].MISC.NOT_SPECIFIED;

				}

				if (comment === null) {
					comment = "";
				}

				userSearchResults += "<div class=\"archive__items reveal\">";

				userSearchResults += '<img style="visibility:hidden;" id="image_' + element.user_id + '" src="./images/' + element.user_image_filepath + '" alt="" data-userImageFilePath="./images/'
					+ element.user_image_filepath + '" data-userComment="' + comment + '" data-companyName="' + companyName
					+ '" data-workLocation="' + workLoc + '" class="image_appended ' + element.company_name + ' ' + element.work_location_name + '">';

				userSearchResults += "</div>";

			});

			if ($('.archive__page:visible').length != 0) {
				$('.search__container .archive__list .allUsers').append(userSearchResults);
			} else {
				$('.archive__list .allUsers').append(userSearchResults);
			}


			$(document).trigger('image_nav_rebind_event');

			var images = $('.image_appended');
			for (let i = 0; i < images.length; i++) {
				const element = images[i];
				element.onload = fadeInImages;
			}

			$('.loader__container').hide();
			toggleModal();
			isReady = true;
		}, 2000);
	} else {
		$(document).trigger('no_image_left');
	}
}

window.fadeInImages = function () {
	$(this).css('visibility', 'visible').hide().fadeIn('slow');
}

window.termsandcondition = function () {
	$(".termsandcondition").on('click', function () {
		if ($(this).is(':checked')) {
			$(".home .button__container").addClass('js--terms');
		} else {
			$(".home .button__container").removeClass('js--terms');
		}
	});
}

window.isDeviceIOS = function () {

	if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
		if (!!window.indexedDB) { return 'iOS 8 and up'; }
		if (!!window.SpeechSynthesisUtterance) { return 'iOS 7'; }
		if (!!window.webkitAudioContext) { return 'iOS 6'; }
		if (!!window.matchMedia) { return 'iOS 5'; }
		if (!!window.history && 'pushState' in window.history) { return 'iOS 4'; }
		return 'iOS 3 or earlier';
	}

	return false;
}

window.computeCameraContainer = function () {
	// var footerHeight = $('footer').height();
	// var captureContHeight = $('.capture__container').height();
	// var documentHeight = $('body').height();

	// var containerHeight = documentHeight - (footerHeight + captureContHeight);

	// $('.btn__capture').css('height', containerHeight + "px");
}

window.fixRange = function () {
	if (!detectmob()) {
		$('.cr-slider-wrap').addClass('range__container');
		$('.cr-slider').addClass('slider__range');
		$('#SELECT_RETAKE_INFO').insertBefore(".cr-slider-wrap");
		$('.cr-slider.slider__range').removeClass('cr-slider');
	}
}

window.autoHeightTextarea = function () {

	$('#comment').each(function () {
		var $this = $(this);
		$this.css('min-height', $this.css('height'));
		$this.css('overflow', 'hidden');
	})
		.on('input paste', function () {
			var $this = $(this);
			var offset = $this.innerHeight() - $this.height();

			if ($this.innerHeight < this.scrollHeight) {
				// Grow the field if scroll height is smaller
				$this.height(this.scrollHeight - offset);
			} else {
				// Shrink the field and then re-set it to the scroll height in case it needs to shrink
				$this.height(1);
				$this.height(this.scrollHeight - offset);
			}
		});
}

$(function () {
	$(document).on('manual_retrieve_scroll_event', function () {
		ctr++;
		isReady = false;
		retrieveScrollResults();
	});

	autoHeightTextarea();

	if (navigator.mediaDevices === undefined || shouldUserAlternative()) {
		//variable from main.js
		if (navigator.mediaDevices === undefined)
			isIE = true;

		if (shouldUserAlternative()) {
			$('video').css('display', 'none !important');
		}

		if ($('.home').length === 0) {
			$.getScript('/assets/js/binarization.js', function (data, textStatus, jqxhr) {
				// console.log(data); // Data returned
				console.log(textStatus); // Success
				console.log(jqxhr.status); // 200
				console.log("Load was performed.");
			});
		}

		$('#picOverlay').hide();


	}


	if (shouldUserAlternative()) {
		$('.capture__container').hide();
		$('.btn__capture').hide();
		$('.attention__container').show();

		$('#imageCropperInstructions').show();

		$('#alternativeImage').attr('src', './assets/img/common/pic_instruction04.png');
		$('#CAPTURE_BACK').hide();
	}

	copyText();
	checkbox();
	termsandcondition();
	scroll();
	toggleNextPage();
	// sampleCameraCapture();
	// animationNTT();
	toggleModal();
	toggleAccordion();
	// toggleCompanyNameSearchField();

	// if (detectmob()) {
	// 	$('.mainUse__container .container__header').click();
	// }

	$('input').attr('autocomplete', 'off');
	$('.use__container').matchHeight();
	if (isDeviceIOS()) {
		$('.btn__save').hide();
	}

	var showNotif = findGetParameter('n');
	if (_.isEmpty(showNotif) || showNotif !== 'false') {
		$('body').addClass('modal__open');
		$('body').addClass('iphone__fix');
		$('.notif__modal').addClass('show');
	}


	//for preventing error in IOS when clicking the reselect button multiple times
	var isUploadButtonClicked = false;
	$('#upload').click(function (e) {
		if (!isUploadButtonClicked) {
			isUploadButtonClicked = true;
			setTimeout(() => {
				isUploadButtonClicked = false;
			}, 1000);
		} else {
			e.preventDefault();
		}
	})


});
