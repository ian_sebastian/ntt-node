window.currentLocale = 'jp';

window.findGetParameter = function (parameterName) {
    var result = null,
        tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

$(function () {
    var lang;

    lang = findGetParameter('lang');
    if (lang !== null) {
        currentLocale = lang;
    }

    var $localeBtn = $('.locale_change');
    var language = LANGUAGES[currentLocale];

    var $shootingLink = $('#shooting_link');
    var $selectLink = $('#select_link');
    var $backTop = $('.back-top');

    var $enBtn = $('.lang__en');
    var $jpBtn = $('.lang__jp');

    $backTop.attr('href', '/?n=false&lang=' + currentLocale);
    $shootingLink.attr('href', '/shooting?n=false&lang=' + currentLocale);
    $selectLink.attr('href', '/select?n=false&lang=' + currentLocale);

    $localeBtn.click(changeLocaleClick);

    function languageChanged(selectedLanguage) {
        switch (selectedLanguage) {
            case 'en':
                $enBtn.addClass('active');
                $jpBtn.removeClass('active');
                $('.add-padding').addClass('padding-eng');
                $('.archive__container .container__header').addClass('eng');
                break;

            case 'jp':
                $jpBtn.addClass('active');
                $enBtn.removeClass('active');
                $('.add-padding').removeClass('padding-eng');
                $('.archive__container .container__header').removeClass('eng');
                break;

            default:
                $jpBtn.addClass('active');
                $enBtn.removeClass('active');
                $('.add-padding').removeClass('padding-eng');

                break;
        }
    }

    function changeLocaleClick() {
        var selectedLanguage = $(this).data('lang');
        currentLocale = selectedLanguage;

        $shootingLink.attr('href', '/shooting?n=false&lang=' + currentLocale);
        $selectLink.attr('href', '/select?n=false&lang=' + currentLocale);


        languageChanged(selectedLanguage);

        loadLanguage(LANGUAGES[selectedLanguage]);

        //from autocomplete.js file
        if (awesomplete.workLocation !== undefined) {
            initializeAwesomplete('#workingLocationSearchList', '#workingLocationList', 'workLocation');

        }

        if (awesomplete.companyList !== undefined) {
            initializeAwesomplete('#companyArchiveSearchList', '#companyDatalist', 'companyList');

        }

        if (awesomplete.workLocationSave !== undefined) {
            initializeAwesomplete('#workingLocationSaveList', '#workingLocationListSave', 'workLocationSave');

        }

        if (awesomplete.companyListSave !== undefined) {
            initializeAwesomplete('#companySearchList', '#companyDatalist', 'companyListSave');

        }

    }

    function loadLanguage(language) {

        $('.use__container').removeAttr('style');
        $('.searchResults').html(language.MISC.NO_DATA);

        //SELECT CAMERA MODAL
        $('#MISC_MODAL_SELECT_CAMERA').html(language.MISC.MODAL_SELECT_CAMERA);
        $('#frontCam__btn').html(language.MISC.SELECT_FRONT);
        $('#backCam__btn').html(language.MISC.SELECT_BACK);

        //HOME PAGE
        $('.HOME_INFO').html(language.HOME.INFO);
        $('#HOME_HTU_STEP1_A').html(language.HOME.HTU_STEP1_A);
        $('#HOME_HTU_STEP1_B').html(language.HOME.HTU_STEP1_B);
        $('#HOME_HTU_STEP2_A').html(language.HOME.HTU_STEP2_A);
        if (currentLocale === 'en') {
            $('#HOME_HTU_STEP2_A').addClass('en');
            $('#HOME_HTU_STEP3_A').addClass('en');


        } else {
            $('#HOME_HTU_STEP2_A').removeClass('en');
            $('#HOME_HTU_STEP3_A').removeClass('en');


        }
        $('#HOME_HTU_STEP3_A').html(language.HOME.HTU_STEP3_A);

        $('#workLocationArchiveSearch option[value="000"]').html(language.MISC.SELECT_WORK_LOCATION);
        $('#companyArchiveSearchList').attr('placeholder', language.MISC.COMPANY);
        $('#HOME_VISION_IMG').attr('src', language.HOME.VISIONARY_IMG);
        $('#HOME_TRUSTED_IMG').attr('src', language.HOME.TRUSTED_IMG);

        //SELECT PAGE
        $('#SELECT_SELECT_INNER_HEAD').html(language.SELECT.SELECT_INNER_HEAD);
        $('#SELECT_SELECT_CONTENT_HEAD').html(language.SELECT.SELECT_CONTENT_HEAD);

        //SHOOTING PAGE
        $('#SHOOTING_SHOOTING_INNER_HEAD').html(language.SHOOTING.SHOOTING_INNER_HEAD);

        if (!shouldUserAlternative()){
            $('#SHOOTING_SHOOTING_CONTENT_HEAD').html(language.SHOOTING.SHOOTING_CONTENT_HEAD);

        }
        else {
            $('#SHOOTING_SHOOTING_CONTENT_HEAD').html(language.SHOOTING.SHOOTING_CONTENT_HEAD_ALT);
            if(currentLocale === 'en') {
                $('.attention__container').addClass('en');
            }

        }


        $('#ATTENTION_LABEL').attr('src', language.MISC.ATTENTION_LABEL);

        //RETAKE PAGE
        $('#SHOOTING_RETAKE_INFO').html(language.SHOOTING.RETAKE_INFO);

        //REUPLOAD PAGE
        $('#SELECT_RETAKE_INFO').html(language.SELECT.RETAKE_INFO);

        var commentLimit = {
            en: 1000,
            jp: 500,
        };

        //DETAILS PAGE
        $('#SHOOTING_DETAILS_INFO').html(language.SHOOTING.DETAILS_INFO);
        $('#SHOOTING_DETAILS_COMMENT_HEAD').html(language.SHOOTING.DETAILS_COMMENT_HEAD);
        $('#comment').attr('placeholder', replaceText(language.SHOOTING.DETAILS_COMMENT_BODY, { count: commentLimit[currentLocale] }));
        $('.terms__container .container__header').html(language.SHOOTING.TERMS_HEAD);
        $('#SHOOTING_TERMS_CHECK').html(language.SHOOTING.TERMS_CHECK);
        $('#workLocationSearch option[value="000"]').html(language.MISC.SELECT_WORK_LOCATION);
        $('span#COMMENT').html(language.MISC.COMMENT);
        // $('#companySearchList').attr('placeholder', language.MISC.COMPANY);


        //FILTER PAGE
        $('#SHOOTING_FILTER_INFO').html(language.SHOOTING.FILTER_INFO);

        //FILTERED PAGE
        $('#SHOOTING_FILTERED_INFO').html(language.SHOOTING.FILTERED_INFO);

        //SUMMARY PAGE
        $('#SHOOTING_SUMMARY_INFO').html(language.SHOOTING.SUMMARY_INFO);
        $('#SHOOTING_DETAILS_COMMENT_HEAD').html(language.SHOOTING.DETAILS_COMMENT_HEAD);

        //MODAL
        $('#MODAL_QUESTION').html(language.SHOOTING.DETAILS_COMMENT_HEAD);
        $('.work_location_modal_label').html(language.MISC.WORK_LOCATION_MODAL_LABEL);
        $('.company_modal_label').html(language.MISC.COMPANY_MODAL_LABEL);
        $('.comment_modal_label').html(language.MISC.COMMENT_MODAL_LABEL);
        $('.question--span').html(language.SHOOTING.DETAILS_COMMENT_HEAD);

        //ARCHIVE PAGE
        $('#companyArchiveSearchList').attr('placeholder', language.MISC.COMPANY);
        $('#workLocationArchiveSearch option[value="000"]').html(language.MISC.SELECT_WORK_LOCATION);

        // $('#iosCompanySelect, #iosDetailsCompanySelect').attr('placeholder', language.MISC.COMPANY);



        $.each(LANGUAGES[currentLocale].LOCATIONS, function (key, value) {
            $('option.' + key).html(value);
            $('option.' + key).attr('name', value);
            $('img.' + key).attr('data-workLocation', value);
        });

        $.each(LANGUAGES[currentLocale].COMPANIES, function (key, value) {
            $('img.' + key).attr('data-companyName', value);
            $('option.' + key).html(value);
            $('option.' + key).attr('name', value);
        });

        $('.iosCompanySelectListClass').trigger('locale_update');

        $('#TERMS_BUTTON').html(language.HOME.TERMS_BUTTON);
        $('.terms__link').on('click', function () {
            $('.terms__modal').fadeIn();
            $('.terms__modal').scrollTop(0);
            $('body').addClass('modal__open');
            if (isDeviceIOS()) {
                //$('body').addClass('iphone__fix');
            }

        });

        $('#TERMS_OF_USE').html(language.HOME.TERMS_OF_USE);

        if (isDeviceIOS()) {
            $('#SELECT_DOWNLOAD_IMAGE').html(language.SELECT.IOS_DOWNLOAD);

        }



        if (currentLocale === 'en') {

            $('.home__container01').addClass('en');
            $('#TERMS_JP').hide();
            $('#TERMS_EN').show();

            $('.filter__container').addClass('en');
            if (isDeviceIOS()) {
                // $('#TXT_DOWNLOAD').attr('src', './assets/img/common/txt_download_en.svg');
                // $('.txt__download').show();

            } else {
                // $('.txt__download').hide();
            }
            $('#DETAILS_TXT_QUESTION').attr('src', './assets/img/common/txt_question_en.svg');

            if (detectmob()) {
                $('#DETAILS_TXT_QUESTION').addClass('sp');
                $('#DETAILS_TXT_QUESTION').removeClass('pc');

                $('#HOME_EXAMPLE_USE_IMG').attr('src', '/assets/img/top/pic_10_eng_sp.svg');
                $('#HOME_EXAMPLE_USE_IMG').addClass('sp');
                $('#HOME_EXAMPLE_USE_IMG').removeClass('pc');


                $('#HOME_TRUSTED_IMG').attr('src', '/assets/img/top/txt_trustedglobalinnovator_eng_sp.svg');

                $('#STEP3_IMG').attr('src', '/assets/img/top/use_container03_en_sp.svg');

            } else {
                $('#DETAILS_TXT_QUESTION').addClass('pc');
                $('#DETAILS_TXT_QUESTION').removeClass('sp');

                $('#HOME_EXAMPLE_USE_IMG').attr('src', '/assets/img/top/pic_10_eng.svg');
                $('#HOME_EXAMPLE_USE_IMG').addClass('pc');
                $('#HOME_EXAMPLE_USE_IMG').removeClass('sp');

                $('#HOME_TRUSTED_IMG').attr('src', '/assets/img/top/txt_trustedglobalinnovator_eng.svg');
                $('#STEP3_IMG').attr('src', language.HOME.STEP3_IMG);


            }

            $('#TOP_MAIN_TXT').attr('src', '/assets/img/top/top_main_txt_sp_en.svg');


        } else {

            $('.home__container01').removeClass('en');

            $('#TERMS_EN').hide();
            $('#TERMS_JP').show();

            $('.filter__container').removeClass('en');

            $('#DETAILS_TXT_QUESTION').attr('src', './assets/img/common/txt_question.svg');
            $('#HOME_EXAMPLE_USE_IMG').attr('src', '/assets/img/top/pic_10.svg');
            $('#HOME_EXAMPLE_USE_IMG').removeClass('sp');
            $('#HOME_EXAMPLE_USE_IMG').removeClass('pc');
            $('#DETAILS_TXT_QUESTION').removeClass('sp');
            $('#DETAILS_TXT_QUESTION').removeClass('pc');
            if (isDeviceIOS()) {
                // $('#TXT_DOWNLOAD').attr('src', './assets/img/common/txt_download.svg');
                // $('.txt__download').show();

            } else {
                // $('.txt__download').hide();
            }

            $('#HOME_TRUSTED_IMG').attr('src', '/assets/img/top/txt_trustedglobalinnovator.svg');
            $('#STEP3_IMG').attr('src', language.HOME.STEP3_IMG);


            $('#TOP_MAIN_TXT').attr('src', '/assets/img/top/top_main_txt_sp.svg');
        }

        $('#STEP1_IMG').attr('src', language.HOME.STEP1_IMG);
        $('#STEP2_IMG').attr('src', language.HOME.STEP2_IMG);
        // $('#TERMS').html(language.HOME.TERMS);

        setTimeout(function () {
            $('.use__container').matchHeight();

        }, 1000);

    }


    languageChanged(currentLocale);

    loadLanguage(LANGUAGES[currentLocale]);

});