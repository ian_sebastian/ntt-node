var isUpload = false;

$(function () {
    var width = 1000;
    var height = 1000;
    var $binCanvas = $('#canvas_filter')[0];
    var binCanvasCtx = $binCanvas.getContext('2d');
    var $binarizeBtn = $('#btn-binarize');
    var $slider = $('#slider-contrast_change');
    var $uploadBtn = $('#btn-upload');
    var $imgSource;
    var originalImageData;
    var promiseChecker;

    const NUM_LOOPS = 2;

    var imageProgress = 0;


    $binCanvas.setAttribute('width', width);
    $binCanvas.setAttribute('height', height);

    const defaultBinarizationValue = 90;

    $slider.attr('min', '30');
    $slider.attr('max', '150');
    $slider.attr('step', '10');
    $slider.attr('value', defaultBinarizationValue);

    if ((isIE || shouldUserAlternative()) && ($('.shooting__page').length === 0 || shouldUserAlternative())) {
        binCanvasCtx.translate(width, 0);
        binCanvasCtx.scale(-1, 1);
    }


    var binarizeImage = new Image();

    binarizeImage.onload = function () {
        binCanvasCtx.drawImage(binarizeImage, 0, 0, width, height);
        originalImageData = binCanvasCtx.getImageData(0, 0, width, height);
        binarize(defaultBinarizationValue);
    }

    $slider.on('mouseup', function () {
        binarize($slider.val());
    });

    $slider.on('touchend', function () {
        binarize($slider.val());
    });

    $uploadBtn.click(function () {

        var imageSource = toBlue();


        imageProgress = 0;
        startCounter(0, 48);

        if (promiseChecker !== undefined) {
            if (!promiseChecker.isPending()) {
                promiseChecker = uploadImagePromise(imageSource);
            }
        } else {
            promiseChecker = uploadImagePromise(imageSource);
        }

        $('.loader__page').show();
        $('.filter__page').hide();
    });



    $binarizeBtn.click(function () {
        $imgSource = $('#img-temporary');
        $slider.val(defaultBinarizationValue);
        binarizeImage.src = undefined;

        if (isIE && !shouldUserAlternative()) {
            
            if ($(this).attr('data-isupload') !== "upload") {
                binarizeImage.src = $imgSource.attr('src');
                originalImage = $imgSource.attr('src');
                originalImageFlip = true;
            } else {
                if ($uploadCrop !== undefined) {
                    $uploadCrop.croppie('result', {
                        type: 'base64'
                    }).then(function (img) {
                        isUpload = true;
                        binarizeImage.src = img;
                        originalImage = img;
                        originalImageFlip = false;

                    });
                }
            }


            $('.retake__page').hide();
            $('.filter__page').show();
        } else if (shouldUserAlternative()) {

            if ($(this).attr('data-isupload') !== "upload") {
                iosImage.result('base64').then(function (img) {
                    isUpload = true;
                    binarizeImage.src = img;
                    originalImage = img;
                    originalImageFlip = false;
                    $('.retake__page').hide();
                    $('.filter__page').show();
                });
            } else {
                if ($uploadCrop !== undefined) {
                    $uploadCrop.croppie('result', {
                        type: 'base64'
                    }).then(function (img) {
                        isUpload = true;
                        binarizeImage.src = img;
                        originalImage = img;
                        originalImageFlip = false;
                        $('.retake__page').hide();
                        $('.filter__page').show();
                    });
                }
            }

        }
    });

    function copyImageData(ctx, src) {
        var dst = ctx.createImageData(src.width, src.height);
        dst.data.set(src.data);
        return dst;
    }


    function binarize(val) {
        var imageData = copyImageData(binCanvasCtx, originalImageData);

        for (var i = imageData.data.length; i >= 0; i -= 4) {
            var r = imageData.data[i];
            var g = imageData.data[i + 1];
            var b = imageData.data[i + 2];
            var a = imageData.data[i + 3];

            var gray = (0.299 * r + 0.587 * g + 0.114 * b);


            if (gray > val) {
                imageData.data[i] = 255;
                imageData.data[i + 1] = 255;
                imageData.data[i + 2] = 255;
                imageData.data[i + 3] = a;
            } else {
                imageData.data[i] = 0;
                imageData.data[i + 1] = 0;
                imageData.data[i + 2] = 0;
                imageData.data[i + 3] = a;
            }


        }
        binCanvasCtx.putImageData(imageData, 0, 0);
    }

    function toBlue() {
        var blueCanvas = document.createElement('canvas');
        var blueCanvasCtx = blueCanvas.getContext('2d');
        blueCanvas.setAttribute('width', width);
        blueCanvas.setAttribute('height', height);
        blueCanvasCtx.translate(width, 0);
        blueCanvasCtx.scale(-1, 1);
        blueCanvasCtx.drawImage($binCanvas, 0, 0);

        var imageData = blueCanvasCtx.getImageData(0, 0, width, height);


        for (var i = imageData.data.length; i >= 0; i -= 4) {
            var r = imageData.data[i];
            var g = imageData.data[i + 1];
            var b = imageData.data[i + 2];
            var a = imageData.data[i + 3];

            if (r === 0 && g === 0 && b === 0) {
                imageData.data[i] = 206;
                imageData.data[i + 1] = 216;
                imageData.data[i + 2] = 229;
                imageData.data[i + 3] = a;
            } else {
                imageData.data[i] = 255;
                imageData.data[i + 1] = 255;
                imageData.data[i + 2] = 255;
                imageData.data[i + 3] = a;
            }
        }

        blueCanvasCtx.putImageData(imageData, 0, 0);

        return getBase64Image(blueCanvas);

    }

    /**
   * from: https://stackoverflow.com/questions/22172604/convert-image-url-to-base64/22172860
   * @param {Jquery} canvas 
   */
    function getBase64Image(canvas) {
        var dataURL = canvas.toDataURL("image/png");
        return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    }


    function uploadImagePromise(source) {
        return (new Promise(function (resolve, reject) {
            $.ajax({
                method: 'POST',
                url: '/upload',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    image: source,
                    counter: imageProgress
                }),
                dataType: 'json',
                success: function (e) {
                    resolve(e);
                },
                error: function (err) {
                    reject(err);
                }
            });
        }))
            .then(uploadImageProgress)
            .catch(function (err) {
                if (err.status === 422) {
                    var message = JSON.parse(err.responseText).message;
                    alert(LANGUAGES[currentLocale].ERROR.SAVING_FAILED);
                    $('.loader__page').hide();
                    $('.filter__page').show();
                    console.log(message);
                }
            });
    }


    function uploadImageProgress(data) {

        //from loader_counter.js file
        startCounter(50, 98);

        if (data.counter <= (NUM_LOOPS - 1)) {

            imageProgress = data.counter;

            promiseChecker = uploadImagePromise(data.imageBase64);

        } else {
            uploadImageSuccess(data);
        }
    }

    function uploadImageSuccess(data) {
        console.log(data);
        var $imgTextualized = $('.js-img-textualized');

        $imgTextualized.each(function () {
            $(this)[0].src = 'data:image/png;base64,' + data.imageBase64;
        });

        $imgTextualized.attr('data-rawData', data.imageBase64);

        //put the image in the animation also
        $('.summary__page .main__container02 .img__container .img__item .image_user').attr('src', 'data:image/png;base64,' + data.imageBase64);


        $('.loader__page').hide();
        $('.filtered__page.confirm').show();
    }

});