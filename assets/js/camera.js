window.hasOrientationChanged = false;
window.iosImage = undefined;

$(function () {
    var video = document.createElement('video');
    var $videoCanvas = $('#videoCanvas')[0];

    if (isDeviceIOS()) {
        $('#videoCanvas').hide();
        $('#iphoneVideo').show();
        video = document.getElementById('iphoneVideo');
    }

    var $startShootingBtn = $('.js--btn__shooting');
    var $frontFaceCamBtn = $('#frontCam__btn');
    var $backFaceCamBtn = $('#backCam__btn');
    var $selectFrontFaceCamBtn = $('#select_frontCam__btn');
    var $selectBackFaceCamBtn = $('#select_backCam__btn');
    var $shootInitialOkBtn = $('#shootOk__btn');
    var $selectRetakeBtn = $('#select__retake__btn');
    var cameraFacingMode;
    var cameraConstraint = {};
    var $captureBtn = $('#btn-snapshot');
    var $imageContainer = $('#img-temporary');
    var $capturePage = $('.capture__page');
    var $binarizeBtn = $('#btn-binarize');
    var $retakeBtn = $('#shoot__retake__btn');
    var intervalID;

    var $alternativeCamera = $('.alternativeCamera');

    var IECameraLoaded = false;

    var videoCanvasCtx = $videoCanvas.getContext('2d');

    var width = 600;
    var height = 600;

    var canvasParam = {
        sx: null,
        sy: null,
        swidth: null,
        sheight: null,
        x: null,
        y: null,
        width: null,
        height: null
    };
    var $criosCamera = $('#criosCameraInput');

    var renderer;
    var composer;
    var binarizationEffect;
    const defaultThreshold = 0.5;

    $frontFaceCamBtn.on("click", setCameraConstraint);
    $backFaceCamBtn.on("click", setCameraConstraint);
    $selectFrontFaceCamBtn.on("click", setCameraConstraint);
    $selectBackFaceCamBtn.on("click", setCameraConstraint);

    $shootInitialOkBtn.on("click", setCameraConstraintPC);
    $selectRetakeBtn.on("click", setCameraConstraintPC);

    $retakeBtn.click(function () {
        $capturePage.trigger('start_interval');
    });

    var facingMode;



    function setCameraConstraint() {
        facingMode = $(this).data('facingmode');
        cameraConstraint = {
            audio: false,
            video: { facingMode: { exact: $(this).data('facingmode') } }
        };

        var $this = this;

        initCamera(cameraConstraint, function (isReady) {


            if (isReady) {
                if (isDeviceIOS()) {
                    if ($($this).data('facingmode') == "environment") {
                        $("#iphoneVideo").removeClass("video--mirrored");
                    } else {
                        $("#iphoneVideo").addClass("video--mirrored");
                    }



                } else {
                    if ($($this).data('facingmode') == "environment") {
                        $("#videoCanvas").removeClass("video--mirrored");
                    } else {
                        $("#videoCanvas").addClass("video--mirrored");
                    }
                }



                if ($($this).hasClass("reupload")) {
                    $("#upload__selectedFacingMode").attr('data-selectedFacingMode', $($this).data('facingmode'))
                    $('.modal__container').css("display", "none");
                } else {
                    $("#camera__selectedFacingMode").attr('data-selectedFacingMode', $($this).data('facingmode'))
                    $('.modal__container').css("display", "none");

                }
            }
        });

    }

    function setCameraConstraintPC() {

        var clickedButton = $(this).attr("id");
        var $this = this;
        if (!detectmob()) {
            cameraConstraint = {
                audio: false,
                video: { facingMode: "user" }
            };

            initCamera(cameraConstraint, function (isReady) {
                if (isReady) {

                    if ($($this).data('facingmode') == "environment") {
                        $("#videoCanvas").removeClass("video--mirrored");
                    } else {
                        $("#videoCanvas").addClass("video--mirrored");
                    }

                    if ($($this).hasClass("reupload")) {
                        $("#upload__selectedFacingMode").attr('data-selectedFacingMode', $($this).data('facingmode'))
                        $('.modal__container').css("display", "none");
                    } else {
                        $("#camera__selectedFacingMode").attr('data-selectedFacingMode', $($this).data('facingmode'))
                        $('.modal__container').css("display", "none");

                    }
                }
            });



        } else {
            if (clickedButton === "select__retake__btn") {
                if ($("#upload__selectedFacingMode").attr('data-selectedFacingMode') == "") {
                    $('.modal__container').fadeIn();
                } else {
                    $capturePage.trigger('start_interval');
                    $('.retake__page').hide();
                    $capturePage.show(0, capturePageShown);
                }
            } else {
                if (shouldUserAlternative()) {
                    $('.shooting__page').hide();
                    $('.capture__page').show();
                } else {
                    $('.modal__container').fadeIn();

                }
            }
        }
    }


    function cameraSuccess(stream, callback) {


        video.srcObject = stream;
        video.play();

        $capturePage.on('stop_stream', function () {
            stream.getTracks()[0].stop();
            // clearInterval(intervalID);
        });

        $('.retake__page').hide();
        $('.shooting__page').hide();
        $capturePage.show(0, capturePageShown);

        var videoContainerWidth = $("#iosContainer").width();
        $("#iosContainer").height(videoContainerWidth);

        capturePageShown();
        computeCameraContainer();

        callback(true);
    }

    function cameraError(callback) {
        alert(LANGUAGES[currentLocale].ERROR.CAMERA_NOT_ALLOWED);
        callback(false);
    }

    // Video と Audioのキャプチャを開始
    function initCamera(camConstraint, callback) {

        if (navigator.mediaDevices === undefined) {
            Webcam.set({
                width: $('.video__container').width(),
                height: $('.video__container').width(),
                image_format: 'png',
                flip_horiz: true,
                // unfreeze_snap: false,
            });

            Webcam.on('live', function () {
                // $('#cameraIE').addClass('video--mirrored');
                // $('#picOverlay').show();
                IECameraLoaded = true;
                $('#picOverlay').show();


            });

            Webcam.attach('#cameraIE');


            $('.retake__page').hide();
            $('.shooting__page').hide();
            $capturePage.show(0, capturePageShown);

        } else {
            const p = navigator.mediaDevices.getUserMedia(camConstraint);
            p.then(function (stream) {
                cameraSuccess(stream, callback);

                $('.js--btnbckcapture').click(function () {
                    $capturePage.trigger('stop_stream');
                })
            });
            p.catch(function (err) {
                cameraError(callback);
            });
        }


    }

    $capturePage.on('start_interval', function () {
        intervalID = setInterval(function () {
            videoCanvasCtx.drawImage(video, canvasParam.sx, canvasParam.sy, canvasParam.swidth, canvasParam.sheight, canvasParam.x, canvasParam.y, canvasParam.width, canvasParam.height);
        }, 33);

    });


    $capturePage.on('stop_interval', function () {
        clearInterval(intervalID);
    });

    video.addEventListener("loadedmetadata", function (e) {
        var vWidth = this.videoWidth,
            vHeight = this.videoHeight;

        if (vWidth <= vHeight) {
            //the device is a mobile phone
            $videoCanvas.setAttribute('width', vWidth);
            $videoCanvas.setAttribute('height', vWidth);

            canvasParam.sx = 0;
            canvasParam.sy = 0;
            canvasParam.swidth = vWidth;
            canvasParam.sheight = vWidth;
            canvasParam.x = 0;
            canvasParam.y = 0;
            canvasParam.width = vWidth;
            canvasParam.height = vWidth;

        } else {
            $videoCanvas.setAttribute('width', vHeight);
            $videoCanvas.setAttribute('height', vHeight);

            canvasParam.sx = Math.floor((vWidth - vHeight) / 2);
            canvasParam.sy = 0;
            canvasParam.swidth = vHeight;
            canvasParam.sheight = vHeight;
            canvasParam.x = 0;
            canvasParam.y = 0;
            canvasParam.width = vHeight;
            canvasParam.height = vHeight;
        }

        $capturePage.trigger('start_interval');

    }, false);

    $alternativeCamera.click(function () {

        $criosCamera.click();




    });
    $retakeBtn.click(function () {
        if (iosImage !== undefined) {
            iosImage.destroy();
        }

        $criosCamera.val('');
        $criosCamera[0].files = undefined;
    });
    $criosCamera.change(function (e) {
        //when an image is captured
        if (e.target.files && e.target.files[0]) {
            moveImageToCroppie(e.target);
            $('.retake__page').show();
            $('.capture__page').hide();

        }
    });
    $(window).on('orientationchange', function () {

        if (!hasOrientationChanged) {
            hasOrientationChanged = true;
        }

        if ($('.capture__page').is(':visible')) {
            //for camera
            $capturePage.trigger('stop_stream');

            if (facingMode == 'environment') {
                $backFaceCamBtn.click();
            } else {
                $frontFaceCamBtn.click();
            }
        }



        if (iosImage !== undefined) {
            iosImage.destroy();
        }
        moveImageToCroppie($criosCamera[0]);
    });

    $('.js--bck__capture').click(function () {
        if (hasOrientationChanged) {
            //for camera
            $capturePage.trigger('stop_stream');

            if (facingMode == 'environment') {
                $backFaceCamBtn.click();
            } else {
                $frontFaceCamBtn.click();
            }
            hasOrientationChanged = false;
        }

    })

    $('.js--bck__retake').click(function () {
        if(hasOrientationChanged) {
            if (iosImage !== undefined) {
                iosImage.destroy();
            }
            moveImageToCroppie($criosCamera[0]);
            hasOrientationChanged = false;
        }
        
    });

    const moveImageToCroppie = function (inputFile) {

        getOrientation(inputFile.files[0], function (imageBase64, imageOrientation) {
            //adjust the width and height
            var containerWidth = $('.retake__page div div.retake__container .container__inner .image__container').width();

            //create a croppie instance
            iosImage = new Croppie($('#upload-image')[0], {
                enableExif: true,
                viewport: {
                    width: containerWidth,
                    height: containerWidth,
                },
                boundary: {
                    width: containerWidth,
                    height: containerWidth
                },
                enableOrientation: true,
                showZoomer: false,
            });


            iosImage.bind({
                url: imageBase64,
                orientation: imageOrientation,
            });
        })
    }



    function capture() {
        var dataUri;
        if (!isIE) {
            $capturePage.trigger('stop_interval');
            dataUri = $videoCanvas.toDataURL('image/png');
            $imageContainer.attr('src', dataUri);
            $('.retake__page').show();
            $('.capture__page').hide();
        }
        else {
            if (IECameraLoaded) {
                Webcam.snap(function (uri) {
                    originalImage = uri.replace(/^data:image\/(png|jpg);base64,/, "");
                    originalImageFlip = true;
                    $imageContainer.attr('src', uri);
                });

                $('.retake__page').show();
                $('.capture__page').hide();
            }

        }

    }

    function capturePageShown() {
        jQuery('html,body').animate({scrollTop:0},0);
        var $videoContainer = $('.video__container');
        var $overlayContainer = $('.overlay__container');
        var $picOverlay = $('#picOverlay');
        var videoPosition = $videoContainer.position();

        $overlayContainer.css({
            top: videoPosition.top,
            left: -10
        });

        $picOverlay.width($videoContainer.width());
        $picOverlay.height($videoContainer.width());

    }

    $captureBtn.click(capture);

    $(window).resize(adjustSizes);
    $('#shoot__retake__btn').click(adjustSizes);

});

window.adjustSizes = function () {
    var videoContainerWidth = $("#iosContainer").width();
    var $videoContainer = $('.video__container');
    var $picOverlay = $('#picOverlay');
    $("#iosContainer").height(videoContainerWidth);

    // var pcCanvas = $('#videoCanvas');

    // pcCanvas.height(pcCanvas.width());

    $picOverlay.width($videoContainer.width());
    $picOverlay.height($videoContainer.width());
}

window.getOrientation = function (file, callback) {
    var fileReader = new FileReader();
    fileReader.onloadend = function () {
        var base64img = "data:" + file.type + ";base64," + _arrayBufferToBase64(fileReader.result);
        var scanner = new DataView(fileReader.result);
        var idx = 0;
        var value = 1; // Non-rotated is the default
        if (fileReader.result.length < 2 || scanner.getUint16(idx) != 0xFFD8) {
            // Not a JPEG
            if (callback) {
                callback(base64img, value);
            }
            return;
        }
        idx += 2;
        var maxBytes = scanner.byteLength;
        var littleEndian = false;
        while (idx < maxBytes - 2) {
            var uint16 = scanner.getUint16(idx, littleEndian);
            idx += 2;
            switch (uint16) {
                case 0xFFE1: // Start of EXIF
                    var endianNess = scanner.getUint16(idx + 8);
                    // II (0x4949) Indicates Intel format - Little Endian
                    // MM (0x4D4D) Indicates Motorola format - Big Endian
                    if (endianNess === 0x4949) {
                        littleEndian = true;
                    }
                    var exifLength = scanner.getUint16(idx, littleEndian);
                    maxBytes = exifLength - idx;
                    idx += 2;
                    break;
                case 0x0112: // Orientation tag
                    // Read the value, its 6 bytes further out
                    // See page 102 at the following URL
                    // http://www.kodak.com/global/plugins/acrobat/en/service/digCam/exifStandard2.pdf
                    value = scanner.getUint16(idx + 6, littleEndian);
                    maxBytes = 0; // Stop scanning
                    break;
            }
        }
        if (callback) {
            callback(base64img, value);
        }
    }
    fileReader.readAsArrayBuffer(file);
};