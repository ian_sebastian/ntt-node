window.imageItems = undefined;
var previousImage;
var nextImage;
var currentImage;

var $navLeftArrow;
var $navRightArrow;
$(function () {
    $navLeftArrow = $('.nav__arrow--left');
    $navRightArrow = $('.nav__arrow--right');

    $(document).on('image_nav_rebind_event', function () {
        updatePreviousAndNextImages(currentImage);
        initializeNavigationArrows();
    });

    $(document).on('clear_nav_images', function () {
        clearCurrentNextAndPrevious();
    });

    $(document).on('proceed_next_image_event', function () {
        updatePreviousAndNextImages(currentImage);
        proceedToNextImage(nextImage)
    });

    $(document).on('no_image_left', function () {
        $navRightArrow.hide();
    });

    $('.archive__items img').click(archiveImageClickEvent);

    $navLeftArrow.click(navLeftArrowClickEvent);

    $navRightArrow.click(navRightArrowClickEvent);


})

const archiveImageClickEvent = function () {
    currentImage = $(this);
    var nextAndPreviousImages = getPreviousAndNextImage(currentImage);
    previousImage = nextAndPreviousImages.previous;
    nextImage = nextAndPreviousImages.next;

    checkPreviousAndNextImages(previousImage, nextImage);
}


const initializeNavigationArrows = function () {

    $('.archive__items img').off('click');
    $('.archive__items img').click(archiveImageClickEvent);

    $navLeftArrow.off('click');
    $navLeftArrow.click(navLeftArrowClickEvent);

    $navRightArrow.off('click');
    $navRightArrow.click(navRightArrowClickEvent);
}

const clearCurrentNextAndPrevious = function () {
    currentImage = undefined;
    nextImage = undefined;
    previousImage = undefined;

}

const navLeftArrowClickEvent = function () {
    if (previousImage.length > 0) {
        proceedToNextImage(previousImage);
    }
}

const navRightArrowClickEvent = function () {
    if (nextImage.length > 0) {
        proceedToNextImage(nextImage);
    }
}

const proceedToNextImage = function (imageToShow) {
    if (imageToShow.length > 0) {
        updateImageViewModal(imageToShow);
        updatePreviousAndNextImages(imageToShow);
        checkPreviousAndNextImages(previousImage, nextImage);
    } else {
        checkPreviousAndNextImages(previousImage, nextImage);
    }

}

const updatePreviousAndNextImages = function (shownImage) {
    currentImage = shownImage;
    if (shownImage !== undefined) {
        var nextAndPreviousImages = getPreviousAndNextImage(shownImage);
        previousImage = nextAndPreviousImages.previous;
        nextImage = nextAndPreviousImages.next;
    }

}

const checkPreviousAndNextImages = function (previous, next) {
    if (previous.length > 0) {
        $navLeftArrow.css('display', 'block');
    } else {
        $navLeftArrow.hide();
    }

    if (next.length > 0) {
        $navRightArrow.css('display', 'block');
    } else {
        $(document).trigger('manual_retrieve_scroll_event');
    }
}

const updateImageViewModal = function (userImage) {
    var userImagePath = userImage.attr('data-userimagefilepath');
    var userImageComment = userImage.attr('data-usercomment');
    var userImageCompanyName = userImage.attr('data-companyname');
    var userImageWorkLocation = userImage.attr('data-worklocation');


    $('#img-userImage').attr('src', '');
    $('#img-userImage').attr('src', userImagePath);
    $('#td-workLocationModal').html(userImageWorkLocation);
    $('#td-companyModal').html(userImageCompanyName);
    $('#span-userComment').html(userImageComment);

}

const getPreviousAndNextImage = function (currentImage) {
    return {
        previous: currentImage.parent().prev().find('img'),
        next: currentImage.parent().next().find('img')
    }
}


