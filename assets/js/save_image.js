$(function () {
    // $saveBtn = $('#btn-textualized_save');
    // $image = $('.js-img-textualized');

    $('#btn-textualized_save').click(function () {
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        canvas.height = canvas.width = 1000;

        ctx.drawImage($('.js-img-textualized')[0], 0, 0, 1000, 1000);

        var timestamp = moment().tz('Asia/Tokyo').format('MMDDHHmm');

        if (!isDeviceIOS()) {

            canvas.toBlob(function (blob) {
                saveAs(blob, "nttdata_" + timestamp + ".png");
            });
        }
        // } else {
        //     var html;
        //     //html = "<p>You can now save the image below:</p>";
        //     html = "<img src='" + canvas.toDataURL() + "' alt='NTT DATA'/>";
        //     var tab = window.open();
        //     tab.document.write(html);
        // }

    });

});