var startTimestamp;
var $counter;
var currentCount;
var total;

$(function () {
    $counter = $('div.counter');
});

window.startCounter = function (startingCount, totalCount) {

    total = totalCount;
    currentCount = startingCount;

    startTimestamp = undefined;

    $counter.html(currentCount);

    window.requestAnimationFrame(loader);


}


window.loader = function (ts) {
    if (startTimestamp === undefined) {
        startTimestamp = ts;
    }

    if ((ts - startTimestamp) >= 50) {
        currentCount++;
        $counter.html(currentCount);
        
        if (currentCount < total) {
            startTimestamp = ts;
        }
    }

    if (currentCount < total) {
        window.requestAnimationFrame(loader);
    }

}