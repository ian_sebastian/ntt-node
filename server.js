const express = require('express');
const path = require('path');
const http = require('http');
const argv = require('yargs').argv;

const routes = require(path.join(__dirname, '/app/routes'));


//for enabling multiprocessing
var cluster = require('cluster');
var numCPU  = require('os').cpus().length;

//initialize global variables holder
global.myVars = {};

const PORT  = argv.port || 7000;

var app = express();

//expose the public folder
app.use(express.static(path.join(__dirname, '/public')));

//for allowing JSON to be sent in server when making an Ajax POST call
//file limit is 50mb
app.use(express.json({
    limit: '50mb'
}));

//register routes here 
routes.register(app);
 
//check for master process
// if (cluster.isMaster) {
//     console.log(`Master ${process.pid} is running`);
//     //create workers
//     for (let i = 0; i < numCPU; i++) {
//         console.log('' + i + '.' + cluster.fork());
//     }

//     cluster.on('exit', (worker, code, signal) => {
//         console.log(`Worker ${worker.process.pid} died.`);
//     });

// } else {
//     console.log(`Worker ${process.pid} started`);
//     // start server here
    app.listen(PORT, 'localhost', () => {
        console.log(`Server started on port: ${PORT}`);
    });
// }