import { Selector, ClientFunction } from 'testcafe'; // first import testcafe selectors

fixture `Home Page`// declare the fixture
    .page`localhost:5000`;  // specify the start page

const getPageUrl = ClientFunction(() => window.location.href);


//then create a test and place your code there
test('Localization Check (EN): HOME_HTU_STEP1_A', async t => {
    await t
        .click('#locale_change_en')
        .expect(Selector('#HOME_HTU_STEP1_A').innerText).eql('Take a photo of yourself', 'There was an error in Localization');
});


test('Localization Check (EN): HOME_HTU_STEP1_B', async t => {
    await t
        .click('#locale_change_en')
        .expect(Selector('#HOME_HTU_STEP1_B').innerText).eql('Upload \nthe photo of your face', 'There was an error in Localization');
});


test('Localization Check (EN): HOME_HTU_STEP2_A', async t => {
    await t
        .click('#locale_change_en')
        .expect(Selector('#HOME_HTU_STEP2_A').innerText).eql('Select a photo using the 30th original design', 'There was an error in Localization');
});

test('Localization Check (EN): HOME_HTU_STEP3_A', async t => {
    await t
        .click('#locale_change_en')
        .expect(Selector('#HOME_HTU_STEP3_A').innerText).eql('Upload your completed portrait to this special site', 'There was an error in Localization');
});


test('Localization Check (JP): HOME_HTU_STEP1_A', async t => {
    await t
        .click('#locale_change_en')
        .click('#locale_change_jp')
        .expect(Selector('#HOME_HTU_STEP1_A').innerText).eql('自分の顔を撮影', 'There was an error in Localization');
});

test('Localization Check (JP): HOME_HTU_STEP1_B', async t => {
    await t
        .click('#locale_change_en')
        .click('#locale_change_jp')
        .expect(Selector('#HOME_HTU_STEP1_B').innerText).eql('自分の顔が写った\n写真をアップロード', 'There was an error in Localization');
});

test('Localization Check (JP): HOME_HTU_STEP2_A', async t => {
    await t
        .click('#locale_change_en')
        .click('#locale_change_jp')
        .expect(Selector('#HOME_HTU_STEP2_A').innerText).eql('選んだ写真を30周年オリジナルデザインへ', 'There was an error in Localization');
});

test('Localization Check (JP): HOME_HTU_STEP3_A', async t => {
    await t
        .click('#locale_change_en')
        .click('#locale_change_jp')
        .expect(Selector('#HOME_HTU_STEP3_A').innerText).eql('出来上がったあなたのポートレートをスペシャルサイトへアップロード', 'There was an error in Localization');
});



// test('Should redirect to /shooting page', async t => {
//     console.log(getPageUrl())
    
//     await t
//         .click('.btn__camera')
//         .expect(getPageUrl()).contains('localhost');

// })