/**
 * Test scripts for UserModel.js
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/05/28
 */

const expect = require("expect");
const UserModel = require("../../app/Models/UserModel");

describe('UserModel.js', () => {
    /**
     * Test scripts for getUsers method 
     * 
     * getUsers: Retrieve and display select list options
     *           for Company and Work Location
     * 
     * @author Arvin Alipio <aj_alipio@commude.ph>
     * @since 2018/05/28
     */
    describe('#getUsers', () => {
        it('should return an object', (done) => {//if will test async add done
            UserModel.getUsers().then((result) => {
                expect(result).toBeAn("object", "Not an object");
                done();
            })
        })

        it('should return a user object and not null', (done) => {//if will test async add done
            UserModel.getUsers().then((result) => {
                expect(result.users).toBeAn("object", "Not an object")
                    .toExist("user is null");
                done();
            })
        })
    })//End of getUsers test scripts


    /**
     * Test scripts for retrieveCompanyEntries method 
     * 
     * retrieveCompanyEntries: Retrieves companies based
     *                         on Work Location
     * 
     * @author Arvin Alipio <aj_alipio@commude.ph>
     * @since 2018/05/29
     */
    describe('#retrieveCompanyEntries', () => {
        it('should return an object if work_location_id is not null and present in db', (done) => {
            let searchItems = {
                work_location_id: 100
            }
            UserModel.retrieveCompanyEntries(searchItems).then((result) => {
                expect(result).toBeAn("object", "Not an object")
                    .toExist("object is null");
                done();
                // console.log(result);
            })
        })

        it('should return a company object if work_location_id is not null and referencing company in the work_location_id is present in db', (done) => {
            let searchItems = {
                work_location_id: 101
            }
            UserModel.retrieveCompanyEntries(searchItems).then((result) => {
                expect(result.COMPANIES).toBeAn("object", "Not an object")
                    .toExist("companies is null");
                done();
                // console.log(result.COMPANIES);
            })
        })

        it('should return an user object if work_location_id is not null and present in db', (done) => {
            let searchItems = {
                work_location_id: 101
            }
            UserModel.retrieveCompanyEntries(searchItems).then((result) => {
                expect(result.USERS).toBeAn("object", "Not an object")
                    .toExist("user is null");
                done();
                // console.log(result.USERS);
            })
        })

        it('should return an empty object of Company and Users if work_location_id is not found', (done) => {
            let searchItems = {
                work_location_id: 999
            }
            UserModel.retrieveCompanyEntries(searchItems).then((result) => {
                expect(result).toBeAn("object", "Not an object");
                done();
                // console.log(result);
            })
        })

        it('should return an empty object of Users if work_location_id is found but referencing company has no users saved', (done) => {
            let searchItems = {
                work_location_id: 100
            }
            UserModel.retrieveCompanyEntries(searchItems).then((result) => {
                expect(result.USERS).toBeAn("object", "Not an object")
                    .toEqual([])
                done();
            })
        })
    })//End of retrieveCompanyEntries test scripts

    /**
     * Test scripts for retrieveScrollResults method 
     * 
     * retrieveScrollResults:  Retrieves users for archive scroll down
     * 
     * @author Arvin Alipio <aj_alipio@commude.ph>
     * @since 2018/05/29
     */
    describe('#retrieveScrollResults', () => {
        it('should return an empty object of users if the query(ies) does not return any data', (done) => {
            let searchItems = {
                work_location_id: 999,
                company_name: ""
            }
            UserModel.retrieveScrollResults(searchItems).then((result) => {
                expect(result.USERS).toBeAn("object", "Not an object")
                                    .toEqual([])
                done();
                // console.log(result.USERS);
            })
        })

        it('should return an empty object of users if the work_location_id and company_name is filled but does not return any data', (done) => {
            let searchItems = {
                work_location_id: 101,
                company_name: "company"
            }
            UserModel.retrieveScrollResults(searchItems).then((result) => {
                expect(result.USERS).toBeAn("object", "Not an object")
                                    .toEqual([])
                done();
                // console.log(result.USERS);
            })
        })

        it('should return an object of users even if company_name is not filled', (done) => {
            let searchItems = {
                work_location_id: 101,
                company_name: ""
            }
            UserModel.retrieveScrollResults(searchItems).then((result) => {
                expect(result.USERS).toBeAn("object", "Not an object")
                                    .toExist("user is null");
                done();
                // console.log(result.USERS);
            })
        })

        it('should return an object of users based on company_name if company_name is filled', (done) => {
            let searchItems = {
                work_location_id: 101,
                company_name: "ommu"
            }
            UserModel.retrieveScrollResults(searchItems).then((result) => {
                expect(result.USERS).toBeAn("object", "Not an object")
                                    .toExist("user is null");
                done();
                // console.log(result.USERS);
            })
        })
    })

    /**
     * Test scripts for searchDbEntries method 
     * 
     * searchDbEntries: Retrieves search results based
     *                  on Company and Work Location
     * 
     * @author Arvin Alipio <aj_alipio@commude.ph>
     * @since 2018/05/29
     */
    describe('#searchDbEntries', () => {
        it('should return an empty object of users if the query(ies) does not return any data', (done) => {
            let searchItems = {
                work_location_id: 999,
                company_name: ""
            }
            UserModel.searchDbEntries(searchItems).then((result) => {
                expect(result.USERS).toBeAn("object", "Not an object")
                                    .toEqual([])
                done();
                // console.log(result.USERS);
            })
        })

        it('should return an empty object of users if the work_location_id and company_name is filled but does not return any data', (done) => {
            let searchItems = {
                work_location_id: 101,
                company_name: "company"
            }
            UserModel.searchDbEntries(searchItems).then((result) => {
                expect(result.USERS).toBeAn("object", "Not an object")
                                    .toEqual([])
                done();
                // console.log(result.USERS);
            })
        })

    })
})