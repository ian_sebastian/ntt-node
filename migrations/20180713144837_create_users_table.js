
exports.up = function (knex, Promise) {
    return knex.schema.createTable('users', table => {
        table.increments('user_id');
        table.string('user_image_filename', 45);
        table.string('user_image_filepath', 150);
        table.string('user_image_original', 150);
        table.text('custom_company');
        table.text('user_comment1');
        table.string('user_comment2', 100);
        table.timestamp('created_at');
        table.timestamp('deleted_at').nullable();
        table.integer('company_id');
        table.integer('work_location_id');
    });
};

exports.down = function (knex, Promise) {
    return knex.schema.dropTableIfExists('users');

};
