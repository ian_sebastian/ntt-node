
exports.up = function (knex, Promise) {
    return knex.schema.createTable('companies', table => {
        table.increments('company_id');
        table.string('company_name', 60);
        table.timestamp('created_at');
    });
};

exports.down = function (knex, Promise) {
    return knex.schema.dropTableIfExists('companies');

};
