
exports.up = function(knex, Promise) {
    return knex.schema.createTable('work_locations', table => {
        table.increments('work_location_id');
        table.string('work_location_name', 60);
        table.timestamp('created_at');
    });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('work_locations');
};
