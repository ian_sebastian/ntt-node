/**
 * WorkLocationModel.js
 * Model for Work Locations table.
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/05/02
 * 
 * @copyright 2018 Commude Philippines, Inc.
 */

const path = require('path');
const { knex } = require(path.resolve(__dirname, '../../config/database'));


/**
 * Retrieve and display select list options
 * for Company and Work Location
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/05/02
 */
const getWorkLocationList = async () => {

    return {
        workLocations: await (knex.select().table('work_locations')),
    };
}

module.exports = {
    getWorkLocationList
};