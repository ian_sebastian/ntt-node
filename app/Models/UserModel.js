/**
 * UserModel.js
 * Model for Users (currently testtable) table.
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/04/26
 * 
 * @copyright 2018 Commude Philippines, Inc.
 */

const path = require('path');
const _ = require('lodash')
const { knex } = require(path.resolve(__dirname, '../../config/database'));
const randomString = require('randomstring');
const imagePath = path.resolve(__dirname, '../../public/images') + '/';
const fs = require('fs');
const initialNumImg = 9;

/**
 * Retrieve and display select list options
 * for Company and Work Location
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/05/02
 */
const getUsers = async () => {
    // let users = await (knex.select().table('users'));

    return {
        users: await (knex.table('users')
            .leftJoin('companies', 'users.company_id', '=', 'companies.company_id')
            .leftJoin('work_locations', 'users.work_location_id', '=', 'work_locations.work_location_id')
            .select([
                'users.user_id',
                'users.user_image_filepath',
                'users.user_comment1',
                'companies.company_name',
                'work_locations.work_location_name',
                'users.custom_company'
            ]).limit(initialNumImg).orderBy('users.created_at', 'desc'))
    };
}

/**
 * Retrieves companies based
 * on Work Location
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/05/07
 */
const retrieveCompanyEntries = async (searchItems) => {

    // console.log(searchItems.work_location_id);
    var users;
    let companies = await (knex.table('companies')
        // .where(
        //     'companies.work_location_id', searchItems.work_location_id
        // )
        .select([
            'companies.company_id',
            'companies.company_name',
        ])
    );

    users = await (knex.table('users')
        .innerJoin('companies', 'users.company_id', '=', 'companies.company_id')

        .select([
            'users.user_id',
            'users.user_image_filepath',
            'users.user_comment1',
            'companies.company_name',
            'users.custom_company'
        ]).limit(initialNumImg).orderBy('users.created_at', 'desc')
    );

    if (searchItems.work_location_id == '999') {
        // users = await (knex.table('users')
        //     .innerJoin('companies', 'users.company_id', '=', 'companies.company_id')
        //     .select([
        //         'users.user_id',
        //         'users.user_image_filepath',
        //         'users.user_comment1',
        //         'companies.company_name'
        //     ]).limit(initialNumImg).orderBy('users.created_at', 'desc'));

        users = await (knex.table('users')
            .leftJoin('companies', 'users.company_id', '=', 'companies.company_id')
            .leftJoin('work_locations', 'users.work_location_id', '=', 'work_locations.work_location_id')
            .select([
                'users.user_id',
                'users.user_image_filepath',
                'users.user_comment1',
                'companies.company_name',
                'work_locations.work_location_name',
                'users.custom_company'

            ]).limit(initialNumImg).orderBy('users.created_at', 'desc')
        );
    }

    return {
        COMPANIES: companies,
        USERS: users
    }
}

const retrieveUserDetailCompanyEntries = async (searchItems) => {
    // console.log(searchItems.work_location_id);
    var users;
    let companies = await (knex.table('companies')
        .where(
            'companies.work_location_id', searchItems.work_location_id
        )
        .select([
            'companies.company_id',
            'companies.company_name',

        ])
    );

    users = await (knex.table('users')
        .innerJoin('companies', 'users.company_id', '=', 'companies.company_id')
        .where(
            'companies.work_location_id', searchItems.work_location_id
        )
        .select([
            'users.user_id',
            'users.user_image_filepath',
            'users.user_comment1',
            'companies.company_name',
            'users.custom_company'
        ]).limit(initialNumImg).orderBy('users.created_at', 'desc')
    );

    if (searchItems.work_location_id == '000') {
        users = await (knex.table('users')
            .innerJoin('companies', 'users.company_id', '=', 'companies.company_id')
            .select([
                'users.user_id',
                'users.user_image_filepath',
                'users.user_comment1',
                'companies.company_name',
                'users.custom_company'
            ]).limit(initialNumImg).orderBy('users.created_at', 'desc'))
    }

    return {
        COMPANIES: companies,
        USERS: users
    }

}

const retrieveScrollResults = async (searchItems) => {
    let users = [];

    // console.log(searchItems);

    let company_id = searchItems.company_id !== undefined && searchItems.company_id !== ''
        ? searchItems.company_id
        : null;

    let work_location_id = searchItems.work_location_id;

    let company_custom = searchItems.custom !== ''
        ? searchItems.custom
        : null;

    // console.log(company_id, work_location_id);

    if (company_id === null && work_location_id !== '000') {
        let knexPromise = knex.table('users')
            .innerJoin('work_locations', 'users.work_location_id', '=', 'work_locations.work_location_id')
            .leftJoin('companies', 'users.company_id', '=', 'companies.company_id')
            .where('users.work_location_id', work_location_id)
            .select([
                'users.user_id',
                'users.user_image_filepath',
                'users.user_comment1',
                'work_locations.work_location_name',
                'companies.company_name',
                'users.custom_company'

            ]).orderBy('users.created_at', 'desc')

        if (company_custom !== null) {
            knexPromise.where('users.custom_company', 'like', '%' + company_custom + '%')
        }

        users = await knexPromise;
    } else if (company_id !== null && work_location_id === '000') {
        users = await (knex.table('users')
            .innerJoin('companies', 'users.company_id', '=', 'companies.company_id')
            .leftJoin('work_locations', 'users.work_location_id', '=', 'work_locations.work_location_id')

            .where(
                'users.company_id', company_id
            )
            .select([
                'users.user_id',
                'users.user_image_filepath',
                'users.user_comment1',
                'companies.company_name',
                'work_locations.work_location_name',
                'users.custom_company'

            ]).orderBy('users.created_at', 'desc')
        );
    } else if (company_id !== null && work_location_id !== '000') {
        users = await (knex.table('users')
            .innerJoin('companies', 'users.company_id', '=', 'companies.company_id')
            .innerJoin('work_locations', 'users.work_location_id', '=', 'work_locations.work_location_id')
            .where(
                'users.company_id', company_id
                // ' users.company_id', 'in', companyId
                // work_location_id: searchItems.workLocationId
            )
            .where('users.work_location_id', work_location_id)
            .select([
                'users.user_id',
                'users.user_image_filepath',
                'users.user_comment1',
                'companies.company_name',
                'work_locations.work_location_name',
                'users.custom_company'

            ]).orderBy('users.created_at', 'desc')
        );
    } else {
        let knexPromise = knex.table('users')
            .leftJoin('companies', 'users.company_id', '=', 'companies.company_id')
            .leftJoin('work_locations', 'users.work_location_id', '=', 'work_locations.work_location_id')
            .select([
                'users.user_id',
                'users.user_image_filepath',
                'users.user_comment1',
                'companies.company_name',
                'work_locations.work_location_name',
                'users.custom_company'


            ]).orderBy('users.created_at', 'desc')

        if (company_custom !== null) {
            knexPromise.where('users.custom_company', 'like', '%' + company_custom + '%')
        }

        users = await knexPromise


    }


    return {
        USERS: users
    };

}


/**
 * Prepares company entries in order to display as options
 * for typeahead functionality 
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/05/10
 * 
 */
const retrieveTypeaheadCompanyEntries = (userInput, workLocationId, res) => {

    let userSearchInput = '%' + userInput + '%';

    knex.table('companies')
        .where(
            'companies.work_location_id', workLocationId
        ).andWhere(
            'companies.company_name', 'like', userSearchInput
        )
        .select([
            'companies.company_id',
            'companies.company_name',
        ]).then(function (rows) {
            var data = [];
            for (i = 0; i < rows.length; i++) {
                data.push(rows[i].company_name);
            }

            res.end(JSON.stringify(data));
        });
}


/**
 * Retrieves search results based
 * on Company and Work Location
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/05/02
 */
const searchDbEntries = async (searchItems) => {


    let company_id = searchItems.company_id !== undefined && searchItems.company_id !== ''
        ? searchItems.company_id
        : null;

    let work_location_id = searchItems.work_location_id;

    let company_custom = searchItems.custom !== ''
        ? searchItems.custom
        : null;

    let users = [];

    if (company_id === null && work_location_id !== '000') {

        let knexPromise = knex.table('users')
            .innerJoin('work_locations', 'users.work_location_id', '=', 'work_locations.work_location_id')
            .leftJoin('companies', 'users.company_id', '=', 'companies.company_id')
            .where('users.work_location_id', work_location_id)
            .select([
                'users.user_id',
                'users.user_image_filepath',
                'users.user_comment1',
                'work_locations.work_location_name',
                'companies.company_name',
                'users.custom_company'

            ]).limit(initialNumImg).orderBy('users.created_at', 'desc')

        if (company_custom !== null) {
            knexPromise.where('users.custom_company', 'like', '%' + company_custom + '%')
        }

        users = await knexPromise;
    } else if (company_id !== null && work_location_id === '000') {
        users = await (knex.table('users')
            .innerJoin('companies', 'users.company_id', '=', 'companies.company_id')
            .leftJoin('work_locations', 'users.work_location_id', '=', 'work_locations.work_location_id')

            .where(
                'users.company_id', company_id
            )
            .select([
                'users.user_id',
                'users.user_image_filepath',
                'users.user_comment1',
                'companies.company_name',
                'work_locations.work_location_name',
                'users.custom_company'


            ]).limit(initialNumImg).orderBy('users.created_at', 'desc')
        );
    } else if (company_id !== null && work_location_id !== '000') {
        users = await (knex.table('users')
            .innerJoin('companies', 'users.company_id', '=', 'companies.company_id')
            .innerJoin('work_locations', 'users.work_location_id', '=', 'work_locations.work_location_id')
            .where(
                'users.company_id', company_id,
            // 'users.custom_company'

            // ' users.company_id', 'in', companyId
            // work_location_id: searchItems.workLocationId
        )
            .where('users.work_location_id', work_location_id)
            .select([
                'users.user_id',
                'users.user_image_filepath',
                'users.user_comment1',
                'companies.company_name',
                'work_locations.work_location_name',
                'users.custom_company'


            ]).limit(initialNumImg).orderBy('users.created_at', 'desc')
        );
    } else {

        let knexPromise = knex.table('users')
            .leftJoin('companies', 'users.company_id', '=', 'companies.company_id')
            .leftJoin('work_locations', 'users.work_location_id', '=', 'work_locations.work_location_id')
            .select([
                'users.user_id',
                'users.user_image_filepath',
                'users.user_comment1',
                'companies.company_name',
                'work_locations.work_location_name',
                'users.custom_company'


            ]).limit(initialNumImg).orderBy('users.created_at', 'desc')

        if (company_custom !== null) {
            knexPromise.where('users.custom_company', 'like', '%' + company_custom + '%')
        }

        users = await knexPromise
    }


    return {
        USERS: users
    }
}

/** 
 * Save form input (User details) to db
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/04/26
 * 
 * @param {object} details 
 */
const saveDetails = async (details) => {
    let dateTime = require('node-datetime');
    let currentDate = dateTime.create().format('y/m/d H:M:S');

    let originalImage = details.original;
    let originalImageFlip = details.original_flip;

    let companyId = !_.isEmpty(details.company_id)
        ? details.company_id
        : null
    let companyCustom = details.company_custom;

    let user_id = (await knex('users')
        .returning('user_id')
        .insert({
            user_image_filename: "NTT IMAGE",
            // user_image_filepath: imageFile + '.png',
            // user_image_original	: imageFile + '_orig.png',
            user_comment1: details.comment,
            created_at: currentDate,
            company_id: companyId,
            custom_company: companyCustom,
            work_location_id: details.work_location_id

        }))[0]


    let buf = Buffer.from(details.image_rawdata, 'base64');
    // let randString = randomString.generate({
    //     length: 20,
    //     charset: 'alphabetic'
    // });
    // let imageFile = randString + '-' + dateTime.create().now();
    let imageFile = user_id;
    fs.writeFile(imagePath + imageFile + '.png', buf, (err) => {
        if (err) {
            throw new Error('error saving image');
        }
    });

    if (!_.isEmpty(originalImage)) {
        let origBuf = Buffer.from(originalImage, 'base64');

        let gm = require('gm').subClass({ imageMagick: true })(origBuf)

        if (originalImageFlip) {
            gm.flop()
        }

        gm.write(imagePath + imageFile + '_orig.png', err => {
            if (err) {
                throw new Error('error saving original image.')
            }
        })

    } else {
        throw new Error('No original Image');
    }


    await knex('users')
        .where('user_id', user_id)
        .update({
            user_image_filepath: imageFile + '.png',
            user_image_original: imageFile + '_orig.png',
        })


    return 'Save Successful';
}


const getCompanyId = async (searchItems) => {

    let companySearchQuery = '%' + searchItems.company_name + '%';
    let companyId = await knex.table('companies')
        .where(
            'company_name', 'like', companySearchQuery
        ).andWhere(
            'work_location_id', '=', searchItems.work_location_id
        ).select([
            'company_id'
        ]);

    return {
        COMPANY_ID: companyId
    }
}

const getAllImages = async () => {
    let rows = await knex.select('user_image_filepath').from('users');

    return rows.map(row => row['user_image_filepath'])
}

module.exports = {
    saveDetails,
    getUsers,
    searchDbEntries,
    retrieveCompanyEntries,
    retrieveUserDetailCompanyEntries,
    retrieveTypeaheadCompanyEntries,
    getAllImages,
    retrieveScrollResults
};