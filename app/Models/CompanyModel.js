/**
 * CompanyModel.js
 * Model for Company table.
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/05/02
 * 
 * @copyright 2018 Commude Philippines, Inc.
 */

const path = require('path');
const { knex } = require(path.resolve(__dirname, '../../config/database'));

/**
 * Retrieve and display Company select list options
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/05/02
 */
const getCompanyList = async () => {
    let companyList = await (knex.select().table('companies'));

    return {
        companyList,
    };
}

module.exports = {
    getCompanyList
};