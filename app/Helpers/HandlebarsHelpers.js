const path = require('path')
const _ = require('lodash')
const fs = require('fs')

const FALLBACK_LANG = 'jp';

const LOCALE_PATH = path.resolve(__dirname, '../../config/lang')

let locale;

let localeData;


const LanguageMiddleware = (req, res, next) => {
    
    locale = req.query.lang || FALLBACK_LANG;

    let localeFile = LOCALE_PATH + '/' + locale + '.js'

    if (fs.existsSync(localeFile)) {
        localeData = require(localeFile);
    } else {
        localeData = require(LOCALE_PATH + '/' + FALLBACK_LANG)
    }

    next();
}


const lang = (val, custom) => {
    // console.log(val, log);

    let path = val;

    if(custom !== undefined && typeof custom !== 'object') {
        path = val + '.' + custom;
    }

    if(_.isEmpty(custom)) {
        return '';
    }

    return _.get(localeData, path, path);
}


module.exports = {
    lang,
    LanguageMiddleware
}