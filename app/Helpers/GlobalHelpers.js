const _ = require('lodash');
const unique = require('unique-random');

const getByKey = (array, key) => {
    return _.head(_.remove(_.map(array, _.property(key)), n => n !== undefined))
}

const copyObject = (obj) => {
    return JSON.parse(JSON.stringify(obj));
}


const buildImageArray = async promiseData => {

    let data = await promiseData();

    let imageArr = _.fill(Array(9), '');

    switch (_.size(data)) {
        case 0:
            return { images: _.fill(imageArr, 'user', 3, 6) };

        case 1:
            return {
                images: imageArr.map((image, i) => {
                    if (i === 3 || i === 4 || i === 5) {
                        return 'user';
                    } else {
                        return data[0];
                    }
                })
            }
        case 2:
            imageArr[0] = data[0];
            imageArr[1] = data[0];
            imageArr[2] = data[0];
            imageArr[3] = 'user';
            imageArr[4] = 'user';
            imageArr[5] = 'user';
            imageArr[6] = data[1];
            imageArr[7] = data[1];
            imageArr[8] = data[1];
            return { images: imageArr };

        default:
            let randomImage = unique(0, data.length - 1);

            return {
                images: imageArr.map((undefined, i) => {
                    if (i !== 4) {
                        return data[randomImage()];
                    } else {
                        return 'user';
                    }
                })
            }
    }
}


const randomProperty = (obj) => {
    var keys = Object.keys(obj)
    return obj[keys[ keys.length * Math.random() << 0]];
};


module.exports = {
    getByKey,
    copyObject,
    buildImageArray,
    randomProperty
}