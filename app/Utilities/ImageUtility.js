const path = require('path');
const Jimp = require('jimp');
const gm = require('gm').subClass({ imageMagick: true });
const random = require('random-number');
const unique = require('unique-random');

const { generateWordList, FONT_PATH, emptyWordList, randWordList, removeWord, newRandom, isListEmpty, listInitialized } = require(path.join(__dirname, '/WordUtility'));
const { copyObject } = require(path.resolve(__dirname, '../Helpers/GlobalHelpers'));
const { getDimensions } = require(path.resolve(__dirname, '../Helpers/CalculationHelper'));


//COLORS
const BLACK = 0x000000FF;
const WHITE = 0xFFFFFFFF;
const BLUE = 0xCED8E5FF;
// const BLUE = 0xD5E9F3FF;
const TEXT_BLUE = '#6485c1';

//IMAGE DIMENSIONS
const IMAGE_HEIGHT = 1000;
const IMAGE_WIDTH = 1000;

//MAXIMUM SIZE OF A TEXT
const MAX_SIZE = 85;//in points


//THE MAXIMUM NUMBER OF COMMANDS THAT THE GM CAN MAKE WITHOUT CAUSING AN ERROR
const GM_LIMIT = 610;

const NUM_LOOPS = 2;

let current_loop = 0;

//THE MINIMUM HEIGHT OF A WORD
const MIN_HEIGHT = 3;

//THE MAXIMUM HEIGHT OF A WORD
const MAX_HEIGHT = 80;

//THE MAXIMUM WIDTH OF A WORD
const MAX_WIDTH = 300;

//THE MARGIN OF EACH WORD
const MARGIN = 8;

//FOUR SIZES OF EACH WORD AND FONTS
const SIZES = [
    [
        60,
        40,
    ],
    [
        20,
        6
    ]
];

const RANGES = [


    [
        { min: 0, max: 1, integer: true },

    ],
    [
        { min: 0, max: 3, integer: true },
        { min: 2, max: 3, integer: true },

    ],

];


//PERCENTAGE COUNTER
let totalPercentageCount = NUM_LOOPS * IMAGE_HEIGHT * IMAGE_WIDTH;
let currentPercentageCount = 0;


const textDistribution = async (buffer, iteration) => {

    //check for a file
    if (buffer === undefined) {
        throw new Error('No file!');
    }

    if (iteration === undefined) {
        throw new Error('No iteration value!');
    }

    //get initial buffer
    let imageBuffer = await getBuffer(undefined, buffer);

    let image;

    let locationsArray;

    current_loop = iteration;

    locationsArray = await getLocations(imageBuffer);

    for (let j = 0; j < locationsArray.length; j++) {

        image = gm(imageBuffer)
            .fill(TEXT_BLUE);

        for (const font in locationsArray[j]) {
            if (locationsArray[j].hasOwnProperty(font)) {
                image.font(FONT_PATH[font]);
                locationsArray[j][font].forEach(text => {
                    putWord(image, text.word, text.x, text.y, text.size);
                });
            }
        }

        imageBuffer = await getBuffer(image);
    }


    return imageBuffer.toString('base64');

}

const saveBuffer = (buffer, filePath) => {
    return new Promise((resolve, reject) => {
        gm(buffer)
            .write(filePath, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
    });
}

const getBuffer = (image, buffer) => {
    return new Promise((resolve, reject) => {
        if (image === undefined) {
            if (buffer !== undefined) {
                gm(buffer)
                    .resize(IMAGE_WIDTH, IMAGE_HEIGHT)
                    .toBuffer((err, buffer) => {
                        if (!err)
                            resolve(buffer);
                        else
                            reject(err);
                    })
            } else {
                reject(new Error('No file path given.'));
            }

        } else {
            image.toBuffer((err, buffer) => {
                if (!err)
                    resolve(buffer);
                else
                    reject(err);
            })
        }
    });
}

const putWord = (image, word, x, y, size) => {
    image.fontSize(size)
        .drawText(x, (size * 0.75) + y, word);
}

const getLocations = (buffer) => {
    return new Promise((resolve, reject) => {
        Jimp.read(buffer).then(image => {
            let wordListArray = randImage[current_loop](image);

            // let wordListArray = scanImage(image);
            // if (current_loop === 0) {
            //     wordListArray = randImage(image);

            // } else {
            //     wordListArray = scanImage(image);
            // }

            resolve(wordListArray);
        }).catch(err => {
            reject(err);
        });
    });
}

const scanImage = (image) => {
    let wordList = copyObject(emptyWordList);

    let text;
    let height = 0;
    let width = 0;
    let dim;
    let count = 0;

    let wordListArray = [];

    let initialX;

    let color;


    for (let y = 0; y < IMAGE_HEIGHT; y++) {

        for (let x = 0; x < IMAGE_WIDTH; x++) {
            currentPercentageCount++;
            color = image.getPixelColor(x, y);

            if (color === BLUE && x !== (IMAGE_WIDTH - 1)) {
                if (initialX === undefined) {
                    initialX = x;
                }
            } else if (color !== BLUE || (x - initialX) >= MAX_WIDTH || x === (IMAGE_WIDTH - 1)) {
                if (initialX !== undefined && x > initialX) {

                    //generate a word here
                    text = generateWordList();

                    //get the dimensions
                    let width = x - initialX;
                    let height = getDimensions({ width }, text.font, text.n).height;

                    if (current_loop > 0) {
                        let check = isFit(initialX, y, width - 2, height, image);


                        if (!check.result) {
                            height = check.height;
                            width = getDimensions({ height }, text.font, text.n).width;
                        }
                    }

                    if (height >= MIN_HEIGHT) {

                        //push the word to the array
                        wordList[text.font].push({
                            word: text.word,
                            size: height,
                            x: initialX,
                            y
                        });


                        removeArea(initialX, y, width, height, image);

                        count++;

                        //stop the loop if it will pass through the limit of gm
                        if (count >= GM_LIMIT) {
                            //push a full copy to the word list array
                            wordListArray.push(copyObject(wordList));

                            //empty word list
                            wordList = copyObject(emptyWordList);

                            //reset count
                            count = 0;
                        }

                    }

                    initialX = undefined;

                }
            }
        }
        initialX = undefined;

    }
    wordListArray.push(copyObject(wordList));

    return wordListArray;

}


const rowScan = image => {
    let wordList = copyObject(emptyWordList);

    let text;
    let height = 0;
    let width = 0;
    let dim;
    let count = 0;

    let wordListArray = [];

    let color;

    // SIZES.forEach(sizes => {
    SIZES[current_loop].forEach(size => {
        for (let y = 0; y < IMAGE_HEIGHT; y += size) {
            for (let x = 0; x < IMAGE_WIDTH; x++) {
                color = image.getPixelColor(x, y);

                if (color === BLUE) {
                    text = generateWordList();

                    height = size;
                    width = getDimensions({ height }, text.font, text.n).width;

                    let check = isFit(x, y, width, height, image);

                    if (check.result) {

                        removeArea(x, y, width, height, image);
                        wordList[text.font].push({
                            word: text.word,
                            size: height,
                            x,
                            y
                        });
                        x = check.x;


                        count++;

                        //stop the loop if it will pass through the limit of gm
                        if (count >= GM_LIMIT) {
                            //push a full copy to the word list array
                            wordListArray.push(copyObject(wordList));

                            //empty word list
                            wordList = copyObject(emptyWordList);

                            //reset count
                            count = 0;
                        }

                    }

                }
            }
        }
    });
    // });


    wordListArray.push(copyObject(wordList));

    return wordListArray;

}


const randImage = {
    1: image => {
        let wordList = copyObject(emptyWordList);

        let text;
        let height = 0;
        let width = 0;
        let dim;
        let count = 0;

        let wordListArray = [];

        let initialX;

        let color;

        let heightIncrement = 1;

        if (current_loop === 0) {
            listInitialized();
            heightIncrement = 40;
        }

        SIZES[current_loop].forEach(size => {
            for (let y = 0; y < IMAGE_HEIGHT; y += heightIncrement) {
                for (let x = 0; x < IMAGE_WIDTH; x++) {

                    if (isListEmpty(size) && current_loop === 0) {
                        break;
                    }


                    // if (current_loop === 0) {
                    //     x = random({ min: 0, max: IMAGE_WIDTH - 200, integer: true })
                    //     y = random({ min: 0, max: IMAGE_HEIGHT - 200, integer: true })
                    // }

                    color = image.getPixelColor(x, y);

                    if (color === BLUE) {
                        text = newRandom(size);

                        height = size;
                        width = text.width;


                        let check = isFit(x, y, width, height, image);

                        if (check.result && x + width < 1000 && y + height < IMAGE_HEIGHT) {

                            removeArea(x, y, width, height, image);
                            //push the word to the array
                            wordList[text.font].push({
                                word: text.word,
                                size: height,
                                x,
                                y
                            });

                            if (current_loop === 0) {
                                removeWord(size, text.word);
                            }

                            count++;

                            //stop the loop if it will pass through the limit of gm
                            if (count >= GM_LIMIT) {
                                //push a full copy to the word list array
                                wordListArray.push(copyObject(wordList));

                                //empty word list
                                wordList = copyObject(emptyWordList);

                                //reset count
                                count = 0;
                            }

                        }

                        x = check.x;


                    }

                    if (isListEmpty(size) && current_loop === 0) {
                        break;
                    }
                }

                if (isListEmpty(size) && current_loop === 0) {
                    break;
                }

            }
        });


        wordListArray.push(copyObject(wordList));

        return wordListArray;
    },

    0: image => {
        let wordList = copyObject(emptyWordList);

        let text;
        let height = 0;
        let width = 0;
        let dim;
        let count = 0;

        let wordListArray = [];

        let initialX;

        let color;

        let heightIncrement = 1;

        if (current_loop === 0) {
            listInitialized();
            heightIncrement = 100;
        }

        let loopCount = 1000000;
        let iteration = 0;

        let x = 0;
        let y = 0;

        let randX = unique(0, IMAGE_WIDTH)
        let randY = unique(0, IMAGE_HEIGHT)

        SIZES[current_loop].forEach(size => {
            // for (let y = 0; y < IMAGE_HEIGHT; y += heightIncrement) {
            //     for (let x = 0; x < IMAGE_WIDTH; x++) {

            // if (isListEmpty(size) && current_loop === 0) {
            //     break;
            // }


            // if (current_loop === 0) {
            //     x = random({ min: 0, max: IMAGE_WIDTH - 200, integer: true })
            //     y = random({ min: 0, max: IMAGE_HEIGHT - 200, integer: true })
            // }

            while (iteration <= loopCount) {
                if (isListEmpty(size) && current_loop === 0) {
                    break;
                }

                x = randX()
                y = randY()

                color = image.getPixelColor(x, y);

                if (color === BLUE) {
                    text = newRandom(size);

                    height = size;
                    width = text.width;


                    let check = isFit(x, y, width, height, image);

                    if (check.result && x + width < IMAGE_WIDTH && y + height < IMAGE_HEIGHT) {

                        removeArea(x, y, width, height, image);
                        //push the word to the array
                        wordList[text.font].push({
                            word: text.word,
                            size: height,
                            x,
                            y
                        });

                        if (current_loop === 0) {
                            removeWord(size, text.word);
                        }

                        count++;

                        //stop the loop if it will pass through the limit of gm
                        if (count >= GM_LIMIT) {
                            //push a full copy to the word list array
                            wordListArray.push(copyObject(wordList));

                            //empty word list
                            wordList = copyObject(emptyWordList);

                            //reset count
                            count = 0;
                        }

                    }

                    // if (current_loop === 0) {
                    //     break;
                    // }

                    // x = check.x;


                }
                iteration++;
            }


            // if (isListEmpty(size) && current_loop === 0) {
            //     break;
            // }


            //     }

            //     if (isListEmpty(size) && current_loop === 0) {
            //         break;
            //     }

            // }
        });


        wordListArray.push(copyObject(wordList));

        return wordListArray;
    }
}


const isFit = (x, y, width, height, image) => {

    let endJ = (height + y);
    let endI = (width + x);

    let currentColor;

    for (let j = y; j < endJ; j++) {
        for (let i = x; i < endI; i++) {

            currentColor = image.getPixelColor(i, j);

            if (x >= IMAGE_WIDTH) {
                return {
                    result: true,
                    x: IMAGE_WIDTH
                }
            }

            if (currentColor !== BLUE) {

                return {
                    result: false,
                    height: j - y,
                    x: endI

                };

            }
        }
    }


    return {
        result: true,
        x: endI
    };
}

const removeArea = (x, y, width, height, image) => {

    for (let i = (y); i < (y + height + MARGIN); i++) {
        for (let j = (x); j < (x + width + MARGIN); j++) {
            image.setPixelColor(WHITE, j, i);
        }

    }

    // x = MARGIN + x;
    // y = MARGIN + y;
    // height += MARGIN;
}



module.exports = {
    textDistribution
};