const unique = require('unique-random');
const random = require('random-number');
const path = require('path')
const _ = require('lodash');

const { copyObject } = require('../Helpers/GlobalHelpers');

// const WORDS = [
//     'Trusted',
//     'Innovator',
//     'Teamwork',
//     'Global',
//     'Ascend',
//     // 'Trusted Global Innovator',
// ];

const WORDS = {
    Trusted: {
        Georgia: 210,
        JosefinSlab: 195,
        KozGo: 210,
        Gotham: 230,
        American: 230,
        Ashley: 170,
        Avenir: 210,
        Clarendon: 245,
        Copperplate: 250,
        Snell: 195,
        Academy: 155,
        Playbill: 105,
        Lucida: 265,
    },
    Global: {
        Georgia: 180,
        JosefinSlab: 170,
        KozGo: 180,
        Gotham: 205,
        American: 185,
        Ashley: 140,
        Avenir: 185,
        Clarendon: 205,
        Copperplate: 220,
        Snell: 160,
        Academy: 165,
        Playbill: 90,
        Lucida: 220,
    },
    //FOR XL
    // Innovator: {
    //     Georgia: 310,
    //     JosefinSlab: 305,
    //     KozGo: 320,
    //     Gotham: 340,
    //     American: 340,
    //     Ashley: 250,
    //     Avenir: 300,
    //     Clarendon: 360,
    //     Copperplate: 360,
    //     Snell: 290,
    //     Academy: 290,
    //     Playbill: 150,
    //     Lucida: 390,
    // },
    Innovator: {
        Georgia: 265,
        JosefinSlab: 255,
        KozGo: 275,
        Gotham: 295,
        American: 295,
        Ashley: 210,
        Avenir: 260,
        Clarendon: 310,
        Copperplate: 305,
        Snell: 245,
        Academy: 245,
        Playbill: 130,
        Lucida: 335,
    },
    'Clients First': {
        Georgia: 230,
        JosefinSlab: 210,
        KozGo: 220,
        Gotham: 250,
        American: 240,
        Ashley: 180,
        Avenir: 215,
        Clarendon: 270,
        Copperplate: 270,
        Snell: 200,
        Academy: 210,
        Playbill: 120,
        Lucida: 260,
    },
    Foresight: {
        Georgia: 170,
        JosefinSlab: 160,
        KozGo: 180,
        Gotham: 200,
        American: 190,
        Ashley: 135,
        Avenir: 170,
        Clarendon: 210,
        Copperplate: 200,
        Snell: 150,
        Academy: 160,
        Playbill: 90,
        Lucida: 210,
    },
    Teamwork: {
        Georgia: 190,
        JosefinSlab: 190,
        KozGo: 200,
        Gotham: 210,
        American: 210,
        Ashley: 150,
        Avenir: 190,
        Clarendon: 220,
        Copperplate: 200,
        Snell: 170,
        Academy: 180,
        Playbill: 100,
        Lucida: 230,
    },
    continuously: {
        Georgia: 118,
        JosefinSlab: 110,
        KozGo: 121,
        Gotham: 128,
        American: 125,
        Ashley: 90,
        Avenir: 116,
        Clarendon: 133,
        Copperplate: 138,
        Snell: 92,
        Academy: 104,
        Playbill: 56,
        Lucida: 140,
    },
    strive: {
        Georgia: 50,
        JosefinSlab: 49,
        KozGo: 53,
        Gotham: 57,
        American: 57,
        Ashley: 41,
        Avenir: 49,
        Clarendon: 61,
        Copperplate: 62,
        Snell: 41,
        Academy: 44,
        Playbill: 24,
        Lucida: 62,
    },
    'resolve every concern': {
        Georgia: 193,
        JosefinSlab: 185,
        KozGo: 201,
        Gotham: 220,
        American: 216,
        Ashley: 147,
        Avenir: 194,
        Clarendon: 221,
        Copperplate: 230,
        Snell: 154,
        Academy: 177,
        Playbill: 103,
        Lucida: 236,
    },
    'your satisfaction': {
        Georgia: 182,
        JosefinSlab: 144,
        KozGo: 155,
        Gotham: 168,
        American: 160,
        Ashley: 120,
        Avenir: 145,
        Clarendon: 175,
        Copperplate: 179,
        Snell: 121,
        Academy: 132,
        Playbill: 75,
        Lucida: 183,
    },
    'ensure your success': {
        Georgia: 180,
        JosefinSlab: 163,
        KozGo: 182,
        Gotham: 200,
        American: 195,
        Ashley: 133,
        Avenir: 179,
        Clarendon: 204,
        Copperplate: 210,
        Snell: 142,
        Academy: 162,
        Playbill: 93,
        Lucida: 210,
    },
    challenges: {
        Georgia: 94,
        JosefinSlab: 91,
        KozGo: 99,
        Gotham: 106,
        American: 102,
        Ashley: 72,
        Avenir: 96,
        Clarendon: 110,
        Copperplate: 114,
        Snell: 72,
        Academy: 84,
        Playbill: 48,
        Lucida: 110,
    },
    ecosystems: {
        Georgia: 102,
        JosefinSlab: 92,
        KozGo: 107,
        Gotham: 117,
        American: 112,
        Ashley: 75,
        Avenir: 105,
        Clarendon: 116,
        Copperplate: 112,
        Snell: 79,
        Academy: 89,
        Playbill: 49,
        Lucida: 116,
    },
    create: {
        Georgia: 54,
        JosefinSlab: 51,
        KozGo: 58,
        Gotham: 63,
        American: 61,
        Ashley: 43,
        Avenir: 57,
        Clarendon: 63,
        Copperplate: 66,
        Snell: 44,
        Academy: 48,
        Playbill: 28,
        Lucida: 66,
    },
    'brighter future': {
        Georgia: 133,
        JosefinSlab: 128,
        KozGo: 139,
        Gotham: 147,
        American: 145,
        Ashley: 111,
        Avenir: 131,
        Clarendon: 156,
        Copperplate: 165,
        Snell: 107,
        Academy: 122,
        Playbill: 66,
        Lucida: 161,
    },
    'We believe': {
        Georgia: 98,
        JosefinSlab: 101,
        KozGo: 101,
        Gotham: 113,
        American: 103,
        Ashley: 75,
        Avenir: 100,
        Clarendon: 110,
        Copperplate: 110,
        Snell: 81,
        Academy: 94,
        Playbill: 52,
        Lucida: 115,
    },
    unique: {
        Georgia: 63,
        JosefinSlab: 60,
        KozGo: 65,
        Gotham: 67,
        American: 65,
        Ashley: 47,
        Avenir: 62,
        Clarendon: 70,
        Copperplate: 69,
        Snell: 50,
        Academy: 58,
        Playbill: 29,
        Lucida: 73,
    },
    together: {
        Georgia: 75,
        JosefinSlab: 72,
        KozGo: 82,
        Gotham: 88,
        American: 83,
        Ashley: 60,
        Avenir: 80,
        Clarendon: 88,
        Copperplate: 91,
        Snell: 62,
        Academy: 68,
        Playbill: 37,
        Lucida: 91,
    },
    'a common goal': {
        Georgia: 137,
        JosefinSlab: 133,
        KozGo: 140,
        Gotham: 157,
        American: 143,
        Ashley: 102,
        Avenir: 139,
        Clarendon: 151,
        Copperplate: 146,
        Snell: 116,
        Academy: 130,
        Playbill: 73,
        Lucida: 162,
    },
    'guide our actions': {
        Georgia: 45,
        JosefinSlab: 45,
        KozGo: 50,
        Gotham: 55,
        American: 50,
        Ashley: 40,
        Avenir: 50,
        Clarendon: 55,
        Copperplate: 55,
        Snell: 40,
        Academy: 45,
        Playbill: 25,
        Lucida: 55,
    },
    'picture the future': {
        Georgia: 45,
        JosefinSlab: 45,
        KozGo: 50,
        Gotham: 55,
        American: 55,
        Ashley: 40,
        Avenir: 46,
        Clarendon: 55,
        Copperplate: 57,
        Snell: 40,
        Academy: 45,
        Playbill: 27,
        Lucida: 55,
    },
    society: {
        Georgia: 20,
        JosefinSlab: 20,
        KozGo: 20,
        Gotham: 23,
        American: 20,
        Ashley: 15,
        Avenir: 20,
        Clarendon: 22,
        Copperplate: 24,
        Snell: 15,
        Academy: 18,
        Playbill: 9,
        Lucida: 23,
    },
    extraordinary: {
        Georgia: 35,
        JosefinSlab: 35,
        KozGo: 38,
        Gotham: 45,
        American: 40,
        Ashley: 32,
        Avenir: 35,
        Clarendon: 45,
        Copperplate: 47,
        Snell: 33,
        Academy: 35,
        Playbill: 20,
        Lucida: 45,
    },
    achieve: {
        Georgia: 20,
        JosefinSlab: 20,
        KozGo: 20,
        Gotham: 25,
        American: 23,
        Ashley: 17,
        Avenir: 20,
        Clarendon: 25,
        Copperplate: 23,
        Snell: 17,
        Academy: 20,
        Playbill: 10,
        Lucida: 25,
    },
};

//PATH LOCATIONS OF EACH FONT
const FONT_PATH = {
    Georgia: path.resolve(__dirname, '../../public/assets/fonts/Georgia.ttf'),
    JosefinSlab: path.resolve(__dirname, '../../public/assets/fonts/JosefinSlab-Bold.ttf'),
    KozGo: path.resolve(__dirname, '../../public/assets/fonts/KozGoPr6N-Bold.otf'),
    Gotham: path.resolve(__dirname, '../../public/assets/fonts/Gotham-BoldItalic.otf'),
    American: path.resolve(__dirname, '../../public/assets/fonts/AmericanTypewriter.ttc'),
    Ashley: path.resolve(__dirname, '../../public/assets/fonts/AshleyScriptMT.otf'),
    Avenir: path.resolve(__dirname, '../../public/assets/fonts/Avenir-LightOblique.otf'),
    Clarendon: path.resolve(__dirname, '../../public/assets/fonts/Clarendon-Bold.otf'),
    Copperplate: path.resolve(__dirname, '../../public/assets/fonts/Copperplate.ttc'),
    Snell: path.resolve(__dirname, '../../public/assets/fonts/SnellRoundhand-Script.otf'),

    //new fonts
    Academy: path.resolve(__dirname, '../../public/assets/fonts/Academy-Engraved.ttf'),
    Playbill: path.resolve(__dirname, '../../public/assets/fonts/Playbill.ttf'),
    Lucida: path.resolve(__dirname, '../../public/assets/fonts/lucida-calligraphy-italic.ttf'),
};

let FONT_CLASSES;

let randWord;
let randFont;

let randRange;

let fontCount;

let emptyWordList = {};

const SIZES = [
    60,
    40,
    20,
    6

];

const newLIST = {
    60: [
        'Trusted',
        'Global',
        'Innovator',
    ],

    40: [
        // 'Innovator',
        'Clients First', 'Foresight', 'Teamwork',
    ],

    20: [
        'continuously', 'strive', 'resolve every concern', 'your satisfaction', 'ensure your success', 'challenges', 'ecosystems', 'create', 'brighter future', 'We believe', 'unique', 'together', 'a common goal'
    ],

    6: [
        'guide our actions', 'picture the future', 'society', 'extraordinary', 'achieve'
    ]
}

let LIST;

const generateWordList = () => {

    let indexWord = randWord();
    let indexFont = fontCount % _.size(FONT_PATH);

    fontCount++;

    return {
        word: WORDS[indexWord],
        n: indexWord,
        font: FONT_CLASSES[indexFont]
    };
}

const randWordList = (range) => {
    let indexWord = randWord();
    let indexFont = randFont();

    return {
        word: WORDS[indexWord],
        n: indexWord,
        font: FONT_CLASSES[indexFont],
        size: SIZES[random(range)]
    };
}



const newRandom = (size) => {

    let randWord = random({
        min: 0,
        max: LIST[size].length - 1,
        integer: true
    });

    let randFont = random({
        min: 0,
        max: FONT_CLASSES.length - 1,
        integer: true
    });

    let word = LIST[size][randWord];
    let font = FONT_CLASSES[randFont];

    return {
        word,
        font,
        width: WORDS[word][font],
    }
}


const removeWord = (size, word) => {
    _.remove(LIST[size], n => n === word);
}


const isListEmpty = size => {
    return _.isEmpty(LIST[size]);
}

const listInitialized = () => {
    LIST = copyObject(newLIST);
}


const initialize = () => {

    randRange = unique(3, 60);

    //inintialize the Font Classes
    FONT_CLASSES = Object.keys(FONT_PATH);

    //initialize random number generators
    randWord = unique(0, WORDS.length - 1);
    randFont = unique(0, FONT_CLASSES.length - 1);


    fontCount = randFont();

    //initialize the empty word list structure
    for (let i = 0; i < FONT_CLASSES.length; i++) {
        emptyWordList[FONT_CLASSES[i]] = [];
    }

}

//initialize the Word Utility when its called
initialize();


module.exports = Object.freeze({
    generateWordList,
    FONT_PATH,
    emptyWordList,
    randWordList,
    removeWord,
    newRandom,
    isListEmpty,
    listInitialized
});

