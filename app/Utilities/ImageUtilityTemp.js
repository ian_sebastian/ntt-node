const path = require('path');
const Jimp = require('jimp');
const gm = require('gm').subClass({ imageMagick: true });
const fs = require('fs');
const _ = require('lodash');

const { generateWordList } = require('./WordUtility');

const BLACK = 0x000000FF;
const WHITE = 0xFFFFFFFF;
const BLUE = 0xD4E8F3FF;
const TEXT_BLUE = '#6585c2';

const IMAGE_HEIGHT = 1920;
const IMAGE_WIDTH = 1080;

const MAX_SIZE = 100;//in points


//the number of limit of request
const GM_LIMIT = 610;

const HEIGHT_WIDTH_RATIO = {
    Phenomena: 2.5,
    Ballw: 1.69,
    Bebas: 2.5,
    Geotica: 1.76
};

//The base margin in points
const MARGIN = {
    top: 1,
    bottom: 1,
    left: 1,
    right: 1,
};

const FONT_PATH = {
    Phenomena: path.resolve(__dirname, '../../public/fonts/Phenomena-Regular.ttf'),
    Ballw: path.resolve(__dirname, '../../public/fonts/ballw.ttf'),
    Bebas: path.resolve(__dirname, '../../public/fonts/BebasNeue-Regular.ttf'),
    Geotica: path.resolve(__dirname, '../../public/fonts/Geotica_2012.ttf')
};


let file = path.resolve(__dirname, '../../public/test.png');
let test = path.resolve(__dirname, '../../public/new.png');
let orig = path.resolve(__dirname, '../../public/orig.png');


const getCoordinates = () => {
    let photo = path.resolve(__dirname, '../../public/ajtest.png');

    return new Promise((resolve, reject) => {
        let coordinates = [];

        Jimp.read(photo, (err, image) => {
            image.resize(IMAGE_WIDTH, IMAGE_HEIGHT);

            for (let y = 0; y < IMAGE_HEIGHT; y++) {
                for (let x = 0; x < IMAGE_WIDTH; x++) {
                    let intValue = image.getPixelColor(x, y);
                    let rgb = Jimp.intToRGBA(intValue);

                    let luminence = (0.2126 * rgb.r) + (0.7152 * rgb.g) + (0.0722 * rgb.b);

                    if (luminence < 140) {
                        coordinates.push([x, y]);
                    }
                }
            }

            resolve({ coordinates });
        });
    });

}


const binarization = () => {
    let file = path.resolve(__dirname, '../../public/ajtest.png');

    getCoordinates().then((data) => {
        let bin = new Jimp(IMAGE_WIDTH, IMAGE_HEIGHT, WHITE, (err, image) => {
            data.coordinates.forEach((coordinate) => {
                image.setPixelColor(BLUE, coordinate[0], coordinate[1]);
            });

            image.write(orig);
        });
    });

}


const textDistribution = async () => {

    // let imageStream = fs.createReadStream(orig);
    // let saveStream = fs.createWriteStream(test);

    let ranges = [

        { min: 80, max: MAX_SIZE, integer: true, marginScale: 50, pos: 'top' },
        { min: 20, max: 60, integer: true, marginScale: 5, pos: 'top' },
        { min: 10, max: 15, integer: true, marginScale: 3, pos: 'top' },

        { min: 20, max: 30, integer: true, marginScale: 3, pos: 'middle' },
        { min: 10, max: 15, integer: true, marginScale: 3, pos: 'middle' },
        { min: 5, max: 10, integer: true, marginScale: 3, pos: 'middle' },

        { min: 20, max: 30, integer: true, marginScale: 3, pos: 'bottom' },
        { min: 10, max: 15, integer: true, marginScale: 3, pos: 'bottom' },
        { min: 5, max: 10, integer: true, marginScale: 3, pos: 'bottom' },


    ];

    let image;
    let imageBuffer = null;

    // Initialize image load and resize
    image = gm(orig)
        .resize(IMAGE_WIDTH, IMAGE_HEIGHT)
        .fill(TEXT_BLUE);

    // Resize and save image first before processing
    image.write(orig, (err) => {
        if (!err) {
            console.log("Image resized. Proceed with image processing")
        } else {
            throw new Error(`Problem during saving file: ${err.message}`);
        }
    });

    image = gm(orig);

    for (let i = 0; i < ranges.length; i++) {

        if (imageBuffer !== null) {
            image = gm(imageBuffer)
        }

        imageBuffer = await saveImageAsBuffer(image);
        let locations = await getLocations(ranges[i], imageBuffer);

        for (const font in locations) {
            if (locations.hasOwnProperty(font)) {
                image.font(FONT_PATH[font]);
                locations[font].forEach(text => {
                    putWord(image, text.word, text.x, text.y, text.size);
                });

            }
        }

        imageBuffer = await saveImageAsBuffer(image);
    }

    image = gm(imageBuffer) // load final image from buffer
    await saveImage(image);

    return 'Text Distribution finished';

}

const saveImageAsBuffer = (image) => {
    return new Promise((resolve, reject) => {

        image.toBuffer(function (err, buffer) {
            if (err) {
                throw err;
            } else {
                console.log("Resolving buffer");
                resolve(buffer);
            }
        });

    });
}

const saveImage = (image) => {
    return new Promise((resolve, reject) => {
        // image.toBuffer('png', (err, buffer) => {
        //     fs.writeFile(orig, buffer, (err) => {
        //         if (err) {
        //             throw new Error('Problem during saving file.');
        //         } else {
        //             resolve('Done Saving');
        //         }
        //     });
        // });

        image.write(orig, (err) => {
            if (!err) {
                resolve('Done');
            } else {
                throw new Error(`Problem during saving file: ${err.message}`);
            }
        });

    });
}

const putWord = (image, word, x, y, size) => {
    image.fontSize(size)
        .drawText(x, (size * 0.75) + y, word);
}


const getLocations = (range, currentImageBuffer) => {

    return new Promise((resolve, reject) => {
        Jimp.read(currentImageBuffer).then(image => {
            let wordList = {
                Phenomena: [],
                Ballw: [],
                Bebas: [],
                Geotica: []
            };

            scanImage(image, wordList, range);

            resolve(wordList);
        });
    });
}

const scanImage = (image, wordList, range) => {
    let text;
    let height = 0;
    let width = 0;
    let dim;
    let count = 0;

    let startX = 0;
    let startY = 0;

    switch (range.pos) {
        case 'middle':
            startX = 0;
            startY = 640;
            break;
        case 'bottom':
            startX = 0;
            startY = 1280;
            break;
        case 'top':
        default:
            startX = 0;
            startY = 0;
    }

    for (let y = startY; y < IMAGE_HEIGHT; y++) {
        for (let x = startX; x < IMAGE_WIDTH; x++) {
            if (image.getPixelColor(x, y) === BLUE) {
                //generate a word here
                text = generateWordList(range);

                dim = computeHeightWidth(text);

                //if a proper color is found loop through it
                //check for the area if fit
                if (isFit(x, y, dim.width, dim.height, range.marginScale, image)) {
                    removeArea(x, y, dim.width, dim.height, range.marginScale, image);
                    wordList[text.font].push({
                        word: text.word,
                        size: text.size,
                        x,
                        y
                    });
                    count++;

                    //stop the loop if it will pass through the limit of gm
                    if (count >= GM_LIMIT) {
                        return;
                    }
                }
            }
        }
    }
}


const computeHeightWidth = (text) => {
    let height = Math.floor(text.size * 0.75);
    let width = Math.floor((text.size / HEIGHT_WIDTH_RATIO[text.font]) * (_.size(text.word)));

    return {
        height,
        width
    };
}


/**
 * This will exclude the area from the image
 * 
 * @param {number} x :starting x coordinate
 * @param {number} y :starting y coordinate
 * @param {number} width :width of the area
 * @param {number} height :height of the area
 * @param {Jimp.image} image :image resource
 * 
 * @author Alvin Generalo
 */
const removeArea = (x, y, width, height, marginScale, image) => {


    for (let j = (y - (MARGIN.top + marginScale)); j < (height + y + (MARGIN.bottom + marginScale)); j++) {
        for (let i = (x - (MARGIN.left + marginScale)); i < (width + x + (MARGIN.right + marginScale)); i++) {
            image.setPixelColor(WHITE, i, j);
        }
    }
}

/**
 * This will check if the current word will fit in the current area
 * 
 * @param {number} x :starting x coordinate
 * @param {number} y :starting y coordinate
 * @param {number} width :width of the area
 * @param {number} height :height of the area
 * @param {Jimp.image} image :image resource
 * 
 * @return {boolean} true if the word will fit
 * 
 * @author Alvin Generalo
 */
const isFit = (x, y, width, height, marginScale, image) => {
    for (let j = (y - (MARGIN.top + marginScale)); j < (height + y + (MARGIN.bottom + marginScale)); j++) {
        for (let i = (x - (MARGIN.left + marginScale)); i < (width + x + (MARGIN.right + marginScale)); i++) {
            if (image.getPixelColor(i, j) !== BLUE) {
                return false;
            }
        }
    }

    return true;
}



module.exports = {
    getCoordinates,
    binarization,
    textDistribution
};