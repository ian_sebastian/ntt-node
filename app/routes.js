const path = require('path');
// var session = require('express-session')
const hbs = require('hbs');

//controllers
const CameraController = require(path.join(__dirname, '/Controllers/CameraController'));
const HomeController = require(path.join(__dirname, '/Controllers/HomeController'));
const ArchiveController = require(path.join(__dirname, '/Controllers/ArchiveController'));
const LanguageController = require(path.join(__dirname, './Controllers/LanguageController'));
const CompanyController = require('./Controllers/CompanyController');

//hbsHelper
const HandlebarsHelper = require(path.join(__dirname, '/Helpers/HandlebarsHelpers'));


const IMAGES_PATH = path.resolve(__dirname, '../public/images')




const register = (app) => {
    /******************************************* CONFIGS ************************************************/
    // set the views directory
    app.set('views', path.join(__dirname, '/Views'));
    //register the partials directory
    hbs.registerPartials(path.join(__dirname, '/Views/partials'));
    //set handlebars as the view engine
    app.set('view engine', 'hbs');


    //register the localization helper
    hbs.registerHelper('lang', HandlebarsHelper.lang);

    /******************************************* START ROUTES *******************************************/

    //enable the use of session
    // app.use(session({
    //     secret: 'mySecret',
    //     resave: false,
    //     saveUninitialized: true,
    //     cookie: { secure: false }
    // }));

    app.use((req,res,next)=> {
        console.log(`Route accessed: ${req.path}`)
        next()
    })

    app.use(HandlebarsHelper.LanguageMiddleware);

    app.get('/', HomeController.getHome);

    app.get('/shooting', HomeController.getShooting);

    app.get('/select', HomeController.getSelect);

    app.get('/change', LanguageController.change);

    app.post('/upload', CameraController.upload);

    app.get('/homeTemp', HomeController.getHomeTemp);
    app.post('/retrieveCompanyEntries', CompanyController.retrieveAllCompany);
    app.post('/retriveAllWorkLoc', CompanyController.retriveAllWorkLoc);
    app.post('/allUsers', ArchiveController.getUsers);
    app.post('/retrieveUserDetailCompanyEntries', ArchiveController.retrieveUserDetailCompanyEntries);
    app.post('/retrieveScrollResults', ArchiveController.retrieveScrollResults);
    app.post('/saveUserDetails', ArchiveController.saveUserDetails);
    app.post('/searchDbEntries', ArchiveController.searchDbEntries);
}

module.exports = {
    register
}