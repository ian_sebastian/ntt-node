const change = (req, res) => {


    if(req.query.locale !== undefined){
        req.session.lang = req.query.locale;
    } else {
        req.session.lang = 'jp';
    }
    
    let backUrl = req.header('Referer') || '/';

    res.redirect(backUrl);

}

module.exports = Object.freeze({
    change
});