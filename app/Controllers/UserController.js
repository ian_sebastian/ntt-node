/**
 * UserController.js
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/04/26
 * 
 * @copyright 2018 Commude Philippines, Inc.
 */

const path      = require('path');
const UserModel = require(path.resolve(__dirname, '../Models/UserModel'));


/**
 * POST: Save user details to db
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/04/26
 * 
 * @param req - Form data/inputs
 * @param res 
 */
const saveDetails = (req, res) => {
    const details = req.body;

    //needs to fix result_image path
    // details['result_img'] = 'test_path';
    UserModel.saveDetails(details);
}

/**
 * POST: Pass search entries based on
 * Company and Work Location dropdown fields
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/05/02
 * 
 * @param req
 * @param res 
 */
const searchDbEntries = (req, res) => {
    const searchItems = req.body;
    let searchResults = UserModel.searchDbEntries(searchItems);

    searchResults.then(function (result) {
        let searchRes = {
            users: result.USERS,
        };

        // console.log(JSON.stringify(searchRes));
        res.end(JSON.stringify(searchRes));
    })
}

/**
 * POST: Send work location id to get Companies in 
 * selected location, triggered by work location
 * change event
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/05/07
 * 
 * @param req
 * @param res 
 */
const retrieveCompanyEntries = (req, res) => {
    const searchItems = req.body;
    const userInput   = req.query.key;
    let searchResults = UserModel.retrieveCompanyEntries(searchItems, userInput);

    searchResults.then(function (result) {
        let searchRes = {
            companies: result.COMPANIES,
            users: result.USERS
        };

        // console.log(JSON.stringify(searchRes));
        res.end(JSON.stringify(searchRes));
    })
}

/**
 * GET: Used to retrieve typeahead searchbox
 * options and display possible choices to
 * the user in realtime
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/05/09
 * 
 * @param req
 * @param res 
 */
const searchCompany = (req, res) => {

    let userInput      = req.query.key;
    let workLocationId = req.query.work_loc_id;

    UserModel.retrieveTypeaheadCompanyEntries(userInput, workLocationId, res);
}

module.exports = {
    saveDetails,
    searchDbEntries,
    retrieveCompanyEntries,
    searchCompany
    // searchDbResults
};