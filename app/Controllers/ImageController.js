const path = require('path');
const fs   = require('fs');
const randomstring  = require('randomstring');
const ImageUtility  = require(path.resolve(__dirname, '../Utilities/ImageUtility'));
const ImageUtilityTemp = require(path.resolve(__dirname, '../Utilities/ImageUtilityTemp'));

const upload = (req, res) => {
    console.log("upload API orig accessed");

    var img = '';
    // SAFE to use than body parser since some characters are being convereted
    // Sample: ("+" => "")

    req.on('data', function (chunk) {
        img += chunk;
    });
    req.on('end', function () {

        // var data = img.replace(/^data:image\/\w+;base64,/, ""); // Backup
        // Slice "data****base64," text and use the trailing part for buffer
        var data = img.slice(img.indexOf(',') + 1).replace(/\s/g, '+');
        
        //before saving generate randomized string for filename and pass also to image utility
        let filename          = randomstring.generate(10) + ".png";
        let binarizedImgPath  = "./public/uploads/" + filename;
        let textualizeImgPath = path.resolve(__dirname, '../../public/uploads/' + filename);

        var buf = new Buffer(data, 'base64');
        fs.writeFile(binarizedImgPath, buf, (err) => {
            if (!err) {
                // ImageUtility.setBaseImage();
                ImageUtility.textDistribution(textualizeImgPath)
                    .then((message) => {
                        console.log(message);
                        //Final Saving is Done here
                        res.send(filename);
                    })
                    .catch((err) => {
                        console.log("distributeTopRegion err");
                        console.log(err);
                    });

                console.log("This will run before the bufferization");
            }
        }); // Save Image
    })
}

const uploadUnbinarized = (req, res) => {
    console.log("upload API accessed");

    var img = '';
    // SAFE to use than body parser since some characters are being convereted
    // Sample: ("+" => "")

    req.on('data', function (chunk) {
        img += chunk;
    });
    req.on('end', function () {

        // var data = img.replace(/^data:image\/\w+;base64,/, ""); // Backup
        // Slice "data****base64," text and use the trailing part for buffer
        var data = img.slice(img.indexOf(',') + 1).replace(/\s/g, '+');
        var now = new Date();

        var baseDirectory = path.resolve(__dirname, '../../public');
        var fileName = 'orig'
                    + now.getTime()
                    + ".png";
        
        var buf = new Buffer(data, 'base64');
        fs.writeFile(baseDirectory + fileName, buf, (err) => {
            if (!err) {

                // ImageUtility.setBaseImage();
                ImageUtility.textDistribution()
                    .then((message) => {
                        console.log(message);
                        //Final Saving is Done here
                        res.send(fileName);
                    })
                    .catch((err) => {
                        console.log("distributeTopRegion err");
                        console.log(err);
                    });

                console.log("This will run before the bufferization");
            }
        }); // Save Image
    })
}

const uploadTemp = (req, res) => {
    console.log("*******TEST******* upload API  accessed *******TEST*******");
   
    var img = '';
    // SAFE to use than body parser since some characters are being convereted
    // Sample: ("+" => "")

    req.on('data', function (chunk) {
        img += chunk;
    });
    req.on('end', function () {

        // var data = img.replace(/^data:image\/\w+;base64,/, ""); // Backup
        // Slice "data****base64," text and use the trailing part for buffer
        var data = img.slice(img.indexOf(',') + 1).replace(/\s/g, '+');

        var buf = new Buffer(data, 'base64');
        fs.writeFile('./public/orig.png', buf); // Save Image
        console.log("Image saved");

        ImageUtilityTemp.binarization({ min: 30, max: 100, integer: true })
            .then((message) => {
                return ImageUtilityTemp.binarization({ min: 11, max: 15, integer: true });
            })
            .then((message) => {
                console.log(message);
                //Final Saving is Done here
                res.send('orig.png');
            })
            .catch((err) => {
                console.log("hey");
                console.log(err);
            });
    })
}

module.exports = {
    upload,
    uploadUnbinarized,
    uploadTemp
    // homeTemp,
};