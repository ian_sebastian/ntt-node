/**
 * ArchiveController.js
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/04/26
 * 
 * @copyright 2018 Commude Philippines, Inc.
 */
const path = require('path');
const UserModel = require(path.resolve(__dirname, '../Models/UserModel'));

/**
 * POST: Save user details to db
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/04/26
 * 
 * @param req - Form data/inputs
 * @param res 
 */
const saveUserDetails = (req, res) => {
    const userDetails = req.body;
    // res.status(422).send({ message: "error" });
    UserModel.saveDetails(userDetails)
        .then(message => {
            res.status(200).send({ message });
        })
        .catch(err => {
            console.log(err);
            res.status(422).send({ message: err.message })
        })
}

/**
 * POST: Send work location id to get Companies in 
 * selected location, triggered by work location
 * change event
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/05/07
 * 
 * @param req
 * @param res 
 */
const retrieveCompanyEntries = (req, res) => {
    const searchItems = req.body;
    let searchResults = UserModel.retrieveCompanyEntries(searchItems);

    searchResults.then(function (result) {
        let searchRes = {
            companies: result.COMPANIES,
            users: result.USERS,
        };

        res.status(200).send(JSON.stringify(searchRes));
    }).catch(err => {
        res.status(422).send({ message: err.message })
    })
}

const retrieveUserDetailCompanyEntries = (req, res) => {
    const searchItems = req.body;
    let searchResults = UserModel.retrieveUserDetailCompanyEntries(searchItems);

    searchResults.then(function (result) {
        let searchRes = {
            companies: result.COMPANIES,
            users: result.USERS,
        };

        res.status(200).send(JSON.stringify(searchRes));
    }).catch(err => {
        res.status(422).send({ message: err.message })
    })
}

const retrieveScrollResults = (req, res) => {
    const searchItems = req.body;

    let scrollResults = UserModel.retrieveScrollResults(searchItems);
    let startIndex = req.body.counter * 9;
    let endIndex = startIndex + 9;

    // console.log(req.body.counter, "start: ", startIndex, "end: ", endIndex);

    scrollResults.then(function (result) {
        let scrollRes = {
            users: result.USERS,
        };

        let test = scrollRes.users.slice(startIndex, endIndex);

        res.status(200).send(JSON.stringify(test));
    }).catch(err => {
        res.status(422).send({ message: err.message })
    })
}

/**
 * POST: Pass search entries based on
 * Company search field and Work Location dropdown
 * 
 * @author Arvin Alipio <aj_alipio@commude.ph>
 * @since 2018/05/02
 * 
 * @param req
 * @param res 
 */
const searchDbEntries = (req, res) => {
    const searchItems = req.body;
    let searchResults = UserModel.searchDbEntries(searchItems);

    searchResults.then(function (result) {
        let searchRes = {
            users: result.USERS,
        };

        res.status(200).send(JSON.stringify(searchRes));
    }).catch(err => {
        res.status(422).send({ message: err.message })
    })
}


const getUsers = (req, res) => {
    UserModel.getUsers()
        .then(data => {
            res.status(200).send({ users:data.users });

        })
        .catch(err => {
            res.status(422).send({ message: err.message })
        })
}


module.exports = {
    saveUserDetails,
    retrieveCompanyEntries,
    retrieveUserDetailCompanyEntries,
    retrieveScrollResults,
    searchDbEntries,
    getUsers
};