const path = require('path')


const { textDistribution } = require('../Utilities/ImageUtility');

const PUBLIC_PATH = path.resolve(__dirname, '../../public');

const home = (req, res) => {
    res.render('master_layout.hbs');
}

const upload = (req, res) => {
    let data = req.body;
    //checker
    if (data.image === undefined) {
        res.status(422).send({ message: 'No file uploaded!' });
    }

    let imageBuffer;

    imageBuffer = Buffer.from(data.image, 'base64');

    console.log('Text Distribution started.')
    console.time('Text Distribution');
    //generate text
    textDistribution(imageBuffer, data.counter)
        .then(newBuffer => {
            //saveImage
            console.timeEnd('Text Distribution');
            console.log('Text Distribution Finished.');

            //return image name
            res.status(200).send({
                imageBase64: newBuffer,
                counter: data.counter + 1
            });
        })
        .catch(err => {
            res.status(422).send({ message: err.message });
        });

}

module.exports = Object.freeze({
    home,
    upload,
})
