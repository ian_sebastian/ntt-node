const { getCompanyList } = require('../Models/CompanyModel');
const { getWorkLocationList } = require('../Models/WorkLocationModel');


const retrieveAllCompany = (req, res) => {
    getCompanyList().then(data => {
        let originalList = data.companyList;
        let list = [];
        let companyListWithId = {};

        originalList.forEach(company => {
            list.push(company.company_name);
            companyListWithId[company.company_name] = company.company_id;
        });

        res.status(200).send({ list, companyListWithId });
    })
        .catch(err => {
            res.status(422).send({ message: err.message })
        })
}

const retriveAllWorkLoc = (req, res) => {
    getWorkLocationList()
        .then(data => {
            let list = {};

            data.workLocations.forEach(locations => {
                list[locations.work_location_id] = locations.work_location_name
            })

            res.status(200).send({ list })
        })
        .catch(err => {
            res.status(422).send({ message: err.message })
        })
}


module.exports = {
    retrieveAllCompany,
    retriveAllWorkLoc
}