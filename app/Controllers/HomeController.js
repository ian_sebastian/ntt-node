const path = require('path');
const _ = require('lodash')

const { buildImageArray } = require(path.resolve(__dirname, '../Helpers/GlobalHelpers'));
const { getWorkLocationList } = require(path.resolve(__dirname, '../Models/WorkLocationModel'));
const { getAllImages, getUsers } = require('../Models/UserModel');
const { getCompanyList } = require('../Models/CompanyModel');

const getHomeTemp = (req, res) => {
    console.log('/home route accessed.', `Worker: ${process.pid}`);
    res.render('master_layout_temp.hbs');
}

const getIndexView = (req, res) => {
    res.render('index.hbs');
}


const IMAGES = [
    'test-1.png',
    // 'test-2.png',
    // 'test-3.png',
    // 'test-4.png',
    // 'test-5.png',
    // 'test-6.png',
    // 'test-7.png',
]


const getHome = async (req, res) => {
    Promise.all([
        getWorkLocationList(),
        getUsers(),
        getCompanyList()
    ]).then((result) => {
        let data = {
            workLocations: null,
            users: null,
            companyList: null            
        }

        result.forEach(obj => {
            _.merge(data, obj);
        });
        res.render('home_page.hbs', data);

    });


}

const getSelect = (req, res) => {

    Promise.all([
        buildImageArray(getAllImages),
        getWorkLocationList(),
        getUsers(),
        getCompanyList()
    ]).then((result) => {
        let data = {
            images: null,
            workLocations: null,
            users: null,
            companyList: null
        }

        result.forEach(obj => {
            _.merge(data, obj);
        });

        res.render('select_page', data);


    });

}


const getShooting = (req, res) => {
    Promise.all([
        buildImageArray(getAllImages),
        getWorkLocationList(),
        getUsers(),
        getCompanyList()
    ]).then((result) => {
        let data = {
            images: null,
            workLocations: null,
            users: null,
            companyList: null
        }

        result.forEach(obj => {
            _.merge(data, obj);
        });

        res.render('shooting_page', data);


    });

}

module.exports = Object.freeze({
    getHome,
    getSelect,
    getShooting,
    getHomeTemp,
    getIndexView
});


