const path = require("path");
const webpack = require("webpack");
const webpack_rules = [];
const webpackOption = {
    entry: {
        //put here the files to compile
        //filename : path_location
        global: "./assets/js/global.js",

        archive: "./assets/js/archive.js",
        autocomplete: "./assets/js/autocomplete.js",
        binarization: "./assets/js/binarization.js",
        // camera_select: "./assets/js/camera_select.js",
        camera: "./assets/js/camera.js",
        image_utility: "./assets/js/image_utility.js",
        languages: "./assets/js/languages.js",
        loader_counter: "./assets/js/loader_counter.js",
        locale: "./assets/js/locale.js",
        main: "./assets/js/main.js",
        save_image: "./assets/js/save_image.js",
        select_page: "./assets/js/select_page.js",
        upload: "./assets/js/upload.js",
        validations: "./assets/js/validations.js",
        "image-navigation": "./assets/js/image-navigation.js",
        
        // video_config: "./assets/js/video_config.js",


    },
    output: {
        path: path.join(__dirname, "/public/assets/js"),
        filename: "[name].js",
    },
    module: {
        rules: webpack_rules
    },
    // plugins: [
    //     new webpack.ProvidePlugin({
    //         $: 'jquery',
    //         _: 'lodash'
    //     }),
    // ]
};
let babelLoader = {
    test: /\.js$/,
    exclude: /(node_modules|bower_components)/,
    use: {
        loader: "babel-loader",
        options: {
            presets: ["@babel/preset-env"]
        }
    }
};
webpack_rules.push(babelLoader);
module.exports = webpackOption;