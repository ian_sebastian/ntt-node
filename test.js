const { textDistribution } = require('./app/Utilities/ImageUtility')
const path = require('path')
var fs = require('fs');


let filePath = path.join(__dirname, '/public/test.png')

console.log(filePath);

let base64 = base64_encode(filePath);

let buf = new Buffer(base64, 'base64')

textDistribution(buf, filePath).then(file => {
    console.log('finish')
})



// function to encode file data to base64 encoded string
function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
}