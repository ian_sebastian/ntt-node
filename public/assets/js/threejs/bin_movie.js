{
  // Boot loader
  $(function () {

    var width = 1000;
    var height = 1000;

    var $imgSrc = $('#img').attr('src');
    var renderer;
    var composer;
    var binarizationEffect;
    const defaultThreshold = 0.35;

    const NUM_LOOPS = 2;

    var imageProgress = 0;

    const BinarizationShader = {
      uniforms: {
        "tDiffuse": {
          type: "t",
          value: null
        },
        "threshold": {
          value: defaultThreshold
        },
      },
      vertexShader: 'varying vec2 vUv; void main() {vUv = uv;gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );}',
      fragmentShader: 'varying vec2 vUv;uniform sampler2D tDiffuse;uniform float threshold;void main() {vec4 color = texture2D(tDiffuse, vUv);float v = color.x * 0.298912 + color.y * 0.586611 + color.z * 0.114478;if (v >= threshold) {v = 1.0;} else {v = 0.0;}gl_FragColor = vec4(vec3(v, v, v), 1.0);}',
      side: THREE.DoubleSide,
      transparent: true
    }

    var mirrorImg;
    $("#selectOk__btn, #shoot__reselect__btn, #select__reselect__btn").on("click", function () {
      mirrorImg = false;
    })

    $("#shootOk__btn, #shoot__retake__btn, #select__retake__btn, #btn-snapshot").on("click", function () {
      if ($('#camera__selectedFacingMode').attr("data-selectedFacingMode") == "environment" || $('#upload__selectedFacingMode').attr("data-selectedFacingMode") == "environment") {
        mirrorImg = false;
      } else {
        mirrorImg = true;
      }
    })



    $("#img-temporary").on("load", function () {
      if (mirrorImg) {
        $("#img-temporary").addClass("video--mirrored");
      } else {
        $("#img-temporary").removeClass("video--mirrored");
      }
    });

    function binInit(userImg, flipImage) {
      $imgSrc = userImg;
      var width;
      var height;
      if ($('#btn-binarize').attr('data-isupload') == "upload") {
        width = 361;
        height = 361;
      } else {
        width = $('#hdn-video_width').val();
        height = $('#hdn-video_height').val();
      }



      const scene = new THREE.Scene()

      // This is also the canvas <canvas></canvas>
      // Preserver Drawing Buffer for true
      renderer = new THREE.WebGLRenderer({
        antialias: true,
        preserveDrawingBuffer: true
      })
      renderer.setSize(width, height)
      renderer.setClearColor(0x000000)
      renderer.setPixelRatio(window.devicePixelRatio)

      renderer.domElement.style.width = '100%';
      renderer.domElement.style.height = '100%';
      renderer.domElement.classList.add('canvas--fit'); // ensure no scroll bar is displayed
      document.getElementById('div-canvas_filter').appendChild(renderer.domElement)

      // Change to make square (1x1) - set width as height
      // const geometry = new THREE.PlaneGeometry(width, height);
      const geometry = new THREE.PlaneGeometry(width, width);

      const texture = (new THREE.TextureLoader()).load($imgSrc);
      texture.anisotropy = renderer.capabilities.getMaxAnisotropy();
      // const material = new THREE.MeshBasicMaterial( {map: videoTexture} );
      const material = new THREE.MeshBasicMaterial({
        map: texture,
        side: THREE.DoubleSide
      });
      const plane = new THREE.Mesh(geometry, material);
      plane.position.set(0, 0, 0)

      if (flipImage) {
        plane.scale.x = -1;
      }

      scene.add(plane);

      // camera
      const camera = new THREE.PerspectiveCamera(50, 1, 1, 1000)
      camera.position.set(0, 0, width)
      camera.lookAt(scene.position)

      composer = new THREE.EffectComposer(renderer);
      composer.addPass(new THREE.RenderPass(scene, camera));
      binarizationEffect = new THREE.ShaderPass(BinarizationShader);
      binarizationEffect.renderToScreen = true;
      composer.addPass(binarizationEffect);

      var controls = new THREE.OrbitControls(camera, renderer.domElement)

      // OVERRIDE
      controls.enableRotate = false;
      controls.enableZoom = false;
      // END

      render()

      function render() {
        controls.update()
        renderer.render(scene, camera)
        // composer.render(scene, camera)
        requestAnimationFrame(render)
      }
    }

    //for binarization of uploaded image, and of live camera captured image
    $("#btn-binarize").on('click', function (e) {
      if (!isIE && browserInfo.name !== 'crios') {
        $("#div-canvas_filter").empty();
        $('#slider-contrast_change').val(0.35); // Set slider value to default

        // var uploadedImg = $('#uploadedImage').attr('src');
        var cameraImg = $('#img-temporary').attr('src');
        var flipImage = false;

        if ($('#upload').attr('data-imgupload') !== 'true') {
          flipImage = false;
        }

        if ($('#camera__selectedFacingMode').attr("data-selectedFacingMode") !== undefined && $('#btn-binarize').attr('data-isupload') != "upload") {
          if ($('#camera__selectedFacingMode').attr("data-selectedFacingMode") == "environment") {
            flipImage = false;
          } else if ($('#camera__selectedFacingMode').attr("data-selectedFacingMode") != "environment") {
            flipImage = true;
          }
        }

        if ($('#upload__selectedFacingMode').attr("data-selectedFacingMode") !== undefined && $('#btn-binarize').attr('data-isupload') != "upload") {
          if ($('#upload__selectedFacingMode').attr("data-selectedFacingMode") == "environment") {
            flipImage = false;
          } else if ($('#upload__selectedFacingMode').attr("data-selectedFacingMode") != "environment") {
            flipImage = true;
          }
        }

        if ($('#btn-binarize').attr('data-isupload') == "upload") {
          if ($uploadCrop !== undefined) {
            $uploadCrop.croppie('result', {
              type: 'base64'
            }).then(function (img) {
              binInit(img, flipImage);
              originalImage = img;
              originalImageFlip = false;
              renderer = composer;
              $('.retake__page').hide();
              $('.filter__page').show();
              $('html,body').animate({scrollTop:0},0);
            });
          }
        } else {
          originalImage = cameraImg;
          originalImageFlip = true;
          binInit(cameraImg, flipImage);
          renderer = composer;
          $('.retake__page').hide();
          $('.filter__page').show();
          $('html,body').animate({scrollTop:0},0);
        }
      }
    });

    // var mirrorImg;
    // $("#ok__btn").on("click", function () {
    //   // $("#img-temporary").removeClass("video--mirrored");
    //   // alert("removed mirror");
    //   mirrorImg = false;
    // })

    // $("#retake__btn").on("click", function () {
    //   // if (!($("#image-temporary").hasClass("video-mirrored"))) {
    //   //   $("#img-temporary").addClass("video--mirrored");
    //   //   alert("add mirror");
    //   // }
    //   mirrorImg = true;
    // })

    // $("#reselect__btn").on("click", function () {
    //   // $("#img-temporary").removeClass("video--mirrored");
    //   mirrorImg = false;
    // })

    // $("#img-temporary").on("load", function () {
    //   // $("#img-temporary").delay(500);
    //   if (mirrorImg) {
    //     $("#img-temporary").addClass("video--mirrored");
    //   } else {
    //     $("#img-temporary").removeClass("video--mirrored");
    //   }
    // })


    // EVENT: Contrast change
    $("#slider-contrast_change").on('input', function (e) {
      binarizationEffect.uniforms.threshold.value = e.target.value
    })


    $('#btn-upload').click(function () {
      if (!isIE) {
        var $btnUpload;
        var $img;

        var promiseChecker;

        var $canvasFilter = $('#div-canvas_filter canvas')[0];
        var canvasSource = document.querySelector('canvas'); // 1st canvas or generated by THREEJS
        var imgBlackWhite = $canvasFilter.toDataURL();

        $('.loader__page').show();
        $('html,body').animate({scrollTop:0},0);

        // Binarize as blue
        binarizeImageToBlue(imgBlackWhite)
          .then(function (data) {
            var canvas = $('#div-canvas_binarized canvas')[0];

            var imageSource = getBase64Image(canvas);


            imageProgress = 0;
            startCounter(0, 50);


            if (promiseChecker !== undefined) {
              if (!promiseChecker.isPending()) {
                promiseChecker = uploadImagePromise(imageSource);
              }
            } else {
              promiseChecker = uploadImagePromise(imageSource);
            }
          })
          .catch(function (err) {
            if (err.status === 422) {
              var message = JSON.parse(err.responseText).message;
              console.log(message);
            }
          });
      }


    });

    function uploadImagePromise(source) {
      return (new Promise(function (resolve, reject) {
        $.ajax({
          method: 'POST',
          url: '/upload',
          contentType: 'application/json; charset=utf-8',
          data: JSON.stringify({
            image: source,
            counter: imageProgress
          }),
          dataType: 'json',
          success: function (e) {
            resolve(e);
          },
          error: function (err) {
            reject(err);
          }
        });
      }))
        .then(uploadImageProgress)
        .catch(function (err) {
          if (err.status === 422) {
            var message = JSON.parse(err.responseText).message;
            alert(message);
            $('.filter__page').show();
            $('html,body').animate({scrollTop:0},0);
            console.log(message);
          }


        });
    }

    function uploadImageProgress(data) {

      //from loader_counter.js file
      startCounter(50, 98);

      if (data.counter <= (NUM_LOOPS - 1)) {

        imageProgress = data.counter;

        promiseChecker = uploadImagePromise(data.imageBase64);

      } else {
        uploadImageSuccess(data);
      }
    }


    function uploadImageSuccess(data) {
      // console.log(data);
      var $imgTextualized = $('.js-img-textualized');
      // var fileName = data.fileName
      //   + "?t="
      //   + new Date().getTime();// Add cache breaker using timestamp

      // Update all iamge containers with the new binarized image
      // $imgTextualized.attr('src', fileName);
      $imgTextualized.each(function () {
        $(this)[0].src = 'data:image/png;base64,' + data.imageBase64;
      });

      $imgTextualized.attr('data-rawData', data.imageBase64);

      //put the image in the animation also
      $('.summary__page .main__container02 .img__container .img__item .image_user').attr('src', 'data:image/png;base64,' + data.imageBase64);


      $('.loader__page').hide();
      $('.filtered__page.confirm').show();
    }

    /**
     * from: https://stackoverflow.com/questions/22172604/convert-image-url-to-base64/22172860
     * @param {Jquery} canvas 
     */
    function getBase64Image(canvas) {
      var dataURL = canvas.toDataURL("image/png");
      return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    }

    /** 
     * Convert image to blue
     * 
     * @author Martin Louie Dela Serna
     * @author Alvin Generalo
     * @author AJ Alipio
     * @since  2018/05/16 Initial Release
     * 
     */
    function binarizeImageToBlue(url) {

      return (new Promise(function (resolve, reject) {
        img = new Image();

        function imageLoaded(ev) {

          const SERVER_IMAGE_WIDTH = 1000;
          const SERVER_IMAGE_HEIGHT = 1000;

          var canvas = $('#div-canvas_binarized canvas')[0];
          var context = canvas.getContext("2d");

          // The image loaded in the event
          var currentImg = ev.target;

          // Read the width and height of the canvas  
          // var width = currentImg.width;
          // var height = currentImg.height;

          // Force 1x1 resolution
          var width = SERVER_IMAGE_WIDTH;
          var height = SERVER_IMAGE_HEIGHT;

          // Set canvas height and width to video or image size to maintain
          // whole picture for custom binarization
          canvas.height = height;
          canvas.width = width;

          // Stamp the image on the left of the canvas.
          context.drawImage(currentImg, 0, 0, 1000, 1000);

          // Get all canvas pixel data
          var imageData = context.getImageData(0, 0, width, height);
          var newImageData = context.createImageData(imageData);

          // Draw black coordnates as blue
          for (var i = imageData.data.length; i >= 0; i -= 4) {

            var x = (i / 4) % width;
            var y = Math.floor((i / 4) / width);

            var tone = imageData.data[i] +
              imageData.data[i + 1] +
              imageData.data[i + 2];
            var alpha = imageData.data[i + 3];

            if (alpha < 128 || tone > 128 * 3) {
              // Area not to draw
              newImageData.data[i] =
                newImageData.data[i + 1] = 255;
              newImageData.data[i + 2] = 255;
              newImageData.data[i + 3] = 255;
            } else {
              // Area to draw
              newImageData.data[i] = 206;
              newImageData.data[i + 1] = 216;
              newImageData.data[i + 2] = 229;
              newImageData.data[i + 3] = 255;
            }
          }

          // Put pixel data on canvas.
          context.putImageData(newImageData, 0, 0);

          resolve(1);
        };

        img.onload = imageLoaded; // Bind onload binarization function
        img.src = url; // Link cam equivalent <img>   

      }));

    }

    // Disable right click on black binarized image
    $('body').on('contextmenu', '#div-canvas_binarized', function (e) {
      return false;
    });


  })
}