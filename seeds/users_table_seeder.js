
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        { user_image_filepath: 'NTT_SAMPLE_1.png'},
        { user_image_filepath: 'NTT_SAMPLE_2.png'},
        { user_image_filepath: 'NTT_SAMPLE_3.png'},
        { user_image_filepath: 'NTT_SAMPLE_4.png'},
        { user_image_filepath: 'NTT_SAMPLE_5.png'},
        { user_image_filepath: 'NTT_SAMPLE_6.png'},
        { user_image_filepath: 'NTT_SAMPLE_7.png'},
        { user_image_filepath: 'NTT_SAMPLE_8.png'},
        { user_image_filepath: 'NTT_SAMPLE_9.png'},
      ]);
    });
};
