const argv = require('yargs').argv;
const knexfile = require('../knexfile');

let database = 'development';
if(argv.database && knexfile[argv.database]) {
    database = argv.database;
}
console.log(`Using ${database} database.`);

const KNEX_DB_CONNECTION = require('knex')(knexfile[database]);

module.exports = {
    knex: KNEX_DB_CONNECTION
};