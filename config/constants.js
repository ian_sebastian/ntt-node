const COMPANY_LIST = [
    "Tokyo, Japan",
    "Kyoto, Japan",
    "Yokohama, Japan",
    "Hiroshima, Japan",
    "Sapporo, Japan"
];

const WORK_LOCATION_LIST = [
    "NTT Data",
    "Commude Japan",
    "Commude Philippines",
    "Accenture Philippines"
];

module.exports = Object.freeze({
    COMPANY_LIST,
    WORK_LOCATION_LIST
});
